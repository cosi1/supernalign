# SupeRNAlign

SupeRNAlign is a tool for flexible superposition of RNA 3D structures. Our program implements an iterative algorithm that splits RNA structures into fragments and superimposes them using existing tools (currently R3D Align, SARA, ARTS, LaJolla or SETTER). Finally, the superimposed structures are saved to a .pdb file and a sequence alignment is generated.

## Setup

SupeRNAlign requires:

* [Python 2.6 or 2.7](https://www.python.org/),
* [BioPython](http://biopython.org/wiki/Main_Page),
* [ModeRNA](http://genesilico.pl/moderna) [optionally],
* a superposition tool (currently [R3D Align](http://rna.bgsu.edu/r3dalign/), [SARA](http://structure.biofold.org/sara/download.html), [ARTS](http://bioinfo3d.cs.tau.ac.il/ARTS/) and [LaJolla](http://raphaelbauer.github.io/lajolla/) are supported),
* [X3DNA](http://x3dna.org/).

**IMPORTANT:** Due to the incompatibility of the latest version of R3D Align with our program, the user is requested to download an older version from [our ftp server](ftp://ftp.genesilico.pl/pub/software/supernalign/r3dalign.zip).


To superimpose small fragments, SupeRNAlign may use Smatch, which comes bundled with the package. Smatch requires:

* networkx
* simplejson
* numpy
* scipy
* progressbar


Additionally, Clarnet may be used to cluster the aligned structure. To install Clarnet, initialize and update the submodule:

    $ git submodule init
    $ git submodule update

Clarnet requires:

* ClaRNA
* networkx (<=1.8)
* simplejson
* numpy
* scipy
* mcl

**IMPORTANT:** To run Clarnet, you need to set CLARNA_PATH in <clarna_dir>/get_pairs/get_pairs.py to the path where ClaRNA is installed, and make a symlink to get_pairs.py in the Clarnet directory.

## Configuration

Create config_file.cfg in SupeRNAlign directory. Configuration file should contain the following lines:

    [CONFIGURATION FILE]
    X3DNA_PATH=<full path to X3DNA>
    SUPERIMPOSER_PATH=<full path to the superimposer, e.g. R3D Align>
    USE_SMATCH=(yes|no)
    SMATCH_PATH=<full path to Smatch, usually smatch/ subdirectory of SupeRNAlign>
    USE_CLARNET=(yes|no)

By default, SupeRNAlign is preconfigured to use R3D Align as the superimposer. If you want to use a different tool, you need to edit superaln.py and replace the following:

    from r3d_superimposer import R3D_Superimposer

with the name of the preferred program, e.g.:

    from sara_superimposer import SARA_Superimposer

and then, in Fragment.superimpose():

    sup = R3D_Superimposer(self.pdb_data, TEMP_DIR)

to:

    sup = SARA_Superimposer(self.pdb_data, TEMP_DIR)
    
## Usage

    superaln.py <pdb_file1> <model1_id> <chain1_id> [<pdb_file2>] <model2_id> <chain2_id> [-o <output_name>] [-p]
    
    <pdb_file1> - name of the first input file
	<model1_id> - model ID of the first structure (target) (usually 0)
	<chain1_id> - chain ID of the first structure
	<pdb_file2> - name of the second input file; may be omitted if both structures are in the same file
	<model2_id> - model ID of the second structure
	<chain2_id> - chain ID of the second structure
	-o <output_name> (or --output) - name of the output files (without extensions) (default: supernalign_output)
	-p (or --preliminary) - don't perform preliminary superposition of the input structures; structures are assumed to be already superimposed
	-c (or --clean) - clean, unmodify and renumber input structures
	-u=yes|no (or --use-clarnet=yes|no) - override USE_CLARNET configuration setting
	-a (or --prealign) - prealign structures using prealign.py (see below)
	-b (or --chainB-only) - save only the second (aligned) structure to the output .pdb file

## Useful tools

`pdb3aln.py` - generates sequence alignment based on the superposition of two structures

`r3d_superimposer.py` - interface to R3D Align; produces a .pdb file with aligned structures

`make_coffee.py` - creates a library file for [T-Coffee](http://tcoffee.org/), which can be used to generate a multiple-sequence alignment based on the structural superposition; the first structure provided should be a "guide", with the other structures superimposed on it

`prealign.py` - aligns structures using Smatch and Clarnet, for further processing with SupeRNAlign; using SupeRNAlign's command-line option `-a` is roughly equivalent to:

    prealign.py <pdb_1> <pdb_2> <prealigned_pdb>
	supernalign.py <pdb_1> <model_1> <chain_1> <prealigned_pdb> 0 A -p -c

## Authors

Paweł Piątkowski <ppiatkowski@genesilico.pl>  
Jagoda Jabłońska
