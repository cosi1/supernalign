#!/usr/bin/env python
from pdb3aln import Pdb3align
import sys
from math import sqrt

DIST_THRESHOLD = 26.0

if __name__ == "__main__":

    args = sys.argv[1:]
    if len(args) < 6 or len(args) % 3 != 0:
        exit("USAGE: make_coffee.py <pdb_file1> <model1_id> <chain1_id> <pdb_file2> <model2_id> <chain2_id> ...")
    
    filenames = sys.argv[1::3]
    model_ids = sys.argv[2::3]
    chain_ids = sys.argv[3::3]

    coords = []
    dist_maps = []
    seqs = []
    lengths = []
    seq_names = []
    for i in range(1, len(filenames)):
        aln = Pdb3align()
        chains = aln.align_input(filenames[0], model_ids[0], chain_ids[0],
                 filenames[i], model_ids[i], chain_ids[i], True)
        coord = aln.get_coordinates(chains)
        aln.match_coordinates(coord)
        dist_maps.append(aln.dist_map)
        coords.append(coord[1])
        seq = "".join([j[1] for j in coord[1]])
        seqs.append(seq)
        lengths.append(len(seq))
        seq_names.append(filenames[i].split(".")[0] + "_" + chain_ids[i])

    coordA = coord[0]
    seqA = "".join([j[1] for j in coord[0]])
    lengthA = len(seqA)
    seq_nameA = filenames[0].split(".")[0] + "_" + chain_ids[0]

    def sorting_function(a, b):
        if a[1] > b[1]:
            return 1
        return -1

    print "! TC_LIB_FORMAT_01"
    num_seqs = len(seqs)
    print num_seqs + 1 # number of sequences
    print seq_nameA, lengthA, seqA
    for i in range(num_seqs):
        print seq_names[i], lengths[i], seqs[i]

    for i in range(num_seqs):
        print "#1", i + 2 # numbers of sequences (starting from 1)
    
        for id_A in range(lengthA):
            dist_row = dist_maps[i][id_A]
            best = [(j, dist_row[j]) for j in range(lengths[i])
                    if dist_row[j] < DIST_THRESHOLD]
            best.sort(cmp = sorting_function)
            for b in best:
                print id_A + 1, b[0] + 1, int((DIST_THRESHOLD - b[1]) * 10)

    print "! SEQ_1_TO_N" # seq numbers are starting from 1



