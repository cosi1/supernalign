#!/usr/bin/env python
__VERSION__ = "1.4.0"

from pdb3aln import Pdb3align
from extres import Extres
from r3d_superimposer import R3D_Superimposer
from global_config import X3DNA_PATH, TEMP_DIR, USE_SMATCH, USE_CLARNET
if USE_SMATCH: from smatch_superimposer import Smatch_Superimposer
import numpy
import os, sys, re, time, tempfile, glob, shutil
import argparse
from copy import deepcopy
from sort_clusters import sort_clusters
from itertools import groupby
from operator import itemgetter


HARD_THRESHOLD = 5.5    # distance threshold
MIN_LENGTH = 4          # minimal length of a fragment
MIN_FROZEN_LENGTH = 4   # minimal length of a frozen fragment


def debug_info(msg):
    if args.debug:
        sys.stderr.write(msg)

def sorting_function(x, y):
    x0 = x[0]; x1 = x[1]
    y0 = y[0]; y1 = y[1]
    if x0 is not None and y0 is not None:
        return x0 - y0
    if x1 is not None and y1 is not None:
        return x1 - y1
    return max(x) - max(y)

def frag_distance(frag):
    """
    Returns mean distance between paired residues of a fragment
    (plus penalty for unpaired residues).
    """
    distances = [d if d is not None else 8.5 for d in frag.distances]
    if len(distances) > 0:
        return sum(distances) / len(distances)
    else:
        return 0

def change_chain(pdb_data, model_id, chain_id, new_chain_id):
    """
    Changes chain of a PDB structure passed as raw text.
    Returns the new structure as text.
    """
    from Bio import PDB
    from StringIO import StringIO

    in_file = StringIO(pdb_data)
    p = PDB.PDBParser()
    struct = p.get_structure("xchg", in_file)
    del in_file
    model = struct[int(model_id)]
    chain = model[chain_id]
    chain.id = new_chain_id

    out_file = StringIO()
    pio = PDB.PDBIO()
    pio.set_structure(struct)
    pio.save(out_file)
    return out_file.getvalue()


class InvalidResidueError(Exception):
    pass


class Fragment(object):
    """
    Represents a single or combined two-chain fragment.

    pdb_data - a two-element list containing structural data of the fragment
            in PDB format
    pairs - a list of paired (and unpaired) residues between the two chains
            (gaps are marked with "-")
    distances - distances between the pairs (or None for unpaired residues)
    """

    def __init__(self, pdb_data_A, pdb_data_B, chain1_id, chain2_id,\
                 model1_id, model2_id, prelim, chain1_len=None, chain2_len=None):
        """
        Input:
        pdb_data_A - PDB structural data of the first...
        pdb_data_B - ...and of the second chain
        """
        self.pdb_data = [pdb_data_A, pdb_data_B]
        self.chain1 = chain1_id
        self.chain2 = chain2_id
        self.model1 = model1_id
        self.model2 = model2_id
        self.chain1_len = chain1_len
        self.chain2_len = chain2_len

        if prelim:
            self.pdb_data = self.superimpose()

        self.p3a = Pdb3align()
        #self.p3a.THRESHOLD = 12.0 #XXX
        self.p3a.align(self.pdb_data[0], self.model1, self.chain1,\
                       self.pdb_data[1], self.model2, self.chain2, False)
        self.pairs = self.p3a.wh_path_indices
        self.sequences = self.p3a.sequences
        self.distances = self.p3a.distances

    def __len__(self):
        """
        Returns the number of pairs in the fragment.

        >>> print len(s)
        90
        """
        return len(self.pairs)

    def superimpose(self):
        """
        Superimposes both chains and updates the data (pairs, distances,
        pdb_data).
        """
        if USE_SMATCH and\
        self.chain1_len is not None and self.chain1_len < 9 and\
        self.chain2_len is not None and self.chain2_len < 9:
            debug_info("Using Smatch...\n")
            sup = Smatch_Superimposer(self.pdb_data, TEMP_DIR)
        else:
            sup = R3D_Superimposer(self.pdb_data, TEMP_DIR)
        debug_info("[%s] Superimposing... " % time.strftime("%H:%M:%S"))
        sup.impose(self.model1, self.chain1, self.model2, self.chain2)
        debug_info("done\n")
        self.chain1 = 'A' #if both chains have the same id, we get invalid pdb file after superposition
        self.chain2 = 'B' #so let's change ids to A and B!

        return sup.pdb_data_out


class Superposition(Fragment):
    """
    The main class, including all the structural data for the input structures
    as well as methods for processing them and obtaining the output alignment.

    chainA_seq - sequence of the first chain (string)
    chainB_seq - sequence of the second chain (string)
    frozen_pairs - list of frozen_pairs (see Fragment.pairs)
    output_pdb - output superposition in PDB format
    sequence_alignment - output sequence alignment (list of two strings)
    """

    def __init__(self, pdb_data_A, pdb_data_B, chain1_id, chain2_id, model1_id, model2_id, prelim):
        if not prelim:
            pdb_data_A = change_chain(pdb_data_A, model1_id, chain1_id, "A")
            pdb_data_B = change_chain(pdb_data_B, model2_id, chain2_id, "B")
            chain1_id = "A"
            chain2_id = "B"

        Fragment.__init__(self, pdb_data_A, pdb_data_B, chain1_id, chain2_id,\
                          model1_id, model2_id, prelim)
        self.unsmoothed = self.distances
        self.distances = self.smooth_distances(self.distances) #smooth distances (only once!)
        self.chainA_seq = ""
        self.chainB_seq = ""
        self.frozen_pairs = []
        self.sequence_alignment = None

    def extract_fragment(self, pairs):
        """
        Extracts the residues from both chains and yields a Fragment instance.

        >>> f = s.extract_fragment(((None, 916), (18, 917), (19, 918), (20, None)))
        >>> print len(f)
        4
        >>> print f.pairs
        [(None, 916), (18, 917), (19, 918), (20, None)]

        >>> f_bad = s.extract_fragment(((1, 2), (3, 4), (5, 6)))
        Traceback (most recent call last):
        ...
        InvalidResidueError: No such pair: (1, 2).
        """

        b_res, a_res = zip(*pairs)
        ex_b = Extres()
        ex_a = Extres()

        ex_a.extract_residues(self.pdb_data[0], self.model1, self.chain1, a_res, False)
        ex_b.extract_residues(self.pdb_data[1], self.model2, self.chain2, b_res, False)

        a_res_not_None = [r for r in a_res if r is not None]
        b_res_not_None = [r for r in b_res if r is not None]

        try:
            fragment = Fragment(ex_a.chain_fragment, ex_b.chain_fragment, self.chain1,
                                self.chain2, self.model1, self.model2, True,
                len(a_res_not_None), len(b_res_not_None)) # superimposed fragment
            old_fragment = Fragment(ex_a.chain_fragment, ex_b.chain_fragment, self.chain1,
                self.chain2, self.model1, self.model2, False) # original fragment
            # if the new alignment is worse than original,
            # restore the original fragment (before superposition)
            if frag_distance(fragment) > frag_distance(old_fragment):
                debug_info("Restoring original fragment...\n")
                fragment = old_fragment
        except KeyError as e:
            res_id = pairs[e.args[0]]
            raise InvalidResidueError("No such pair: %s." % (res_id,))

        return fragment

    def smooth_distances(self, list_of_dist, window_len=5):
        """
        Smooth the data using a window with requested size.

        This method is based on the convolution of a scaled window with the signal.
        The signal is prepared by introducing reflected copies of the signal
        (with the window size) in both ends so that transient parts are minimized
        in the beginning and end part of the output signal.

        http://wiki.scipy.org/Cookbook/SignalSmooth
        """
        new_smoothed = []
        c = [a for a in list_of_dist if a is not None]
        if len(c) < window_len:
            return list_of_dist
        s = numpy.r_[c[window_len-1:0:-1], c, c[-1:-window_len:-1]]
        w = numpy.ones(window_len, 'd')
        y = numpy.convolve(w/w.sum(), s, mode = 'valid')
        smoothed = y[(window_len/2-1):-(window_len/2-1)]

        if len(smoothed) < len(c):
            smoothed = c

        cnt = 0

        for i in list_of_dist: #bring back Nones
            if i is None:
                new_smoothed.append(None)
            else:
                new_smoothed.append(smoothed[cnt])
                cnt += 1
        return new_smoothed



    def cmp_frags(self, frag1, frag2):
        """
        Compare two fragments, i.e. [(1,2), (5,6)] and [(56,76), (4,77)]
        to check whether they are consecutive or not.
        """

        for f1 in frag1:
            for f2 in frag2:
                if f2[0] != None and f1[0] != None:
                    if f2[0]+1 == f1[0] or f2[0]-1 == f1[0]:
                        return True
        else:
            return False


    def join_adjacent_fragments(self, fragments):
        """
        Recursively join fragments consecutive in sequence.
        """

        new_fragments = []
        falses = [] 	

        def recursive_adjacent(fragments):

            for frag1 in fragments:
                for frag2 in fragments:
                    if frag1 == frag2: continue
                    join_true = self.cmp_frags(frag1, frag2)
                    falses.append(join_true)
                    if join_true:
                        new_frag = frag1 + frag2
                        joined = [x for x in fragments if x != frag1 and x != frag2]
                        joined.append(new_frag)
                        new_fragments[:] = joined
                        recursive_adjacent(joined)



        recursive_adjacent(fragments)

        if True not in falses:
            return fragments
        else:
            return new_fragments						


    def find_fragments(self, list_of_combined):
        """
        Finds fragments basing on distances.
        Returns a list of pair lists.
        """

        median_thr = calc_med_dist(self.distances)
        threshold = median_thr if median_thr < HARD_THRESHOLD else HARD_THRESHOLD

        debug_info("Threshold: %s\n" % threshold)

        pairs_list = []
        pair_indices = [] # indices of the candidates (in self.pairs)
        frags = [] # Fragments list


        smoothed = self.distances

        flags = [-1 if p in self.frozen_pairs else 0 for p in self.pairs]
        # flags make a "map" of all pairs
        # -1 - pair is already frozen
        #  0 - pair is neither frozen nor a part of a fragment
        # >0 - pair is incorporated into fragment number N (1-based!)

        if list_of_combined==[]:
            comb = ((), ())
        else:
            comb = zip(*list_of_combined)[1:3]
            comb = [[int(c) for c in d] for d in comb]

        n_res = 0
        frag_num = 1
        self.pairs.append((None, None)) # add terminator
        smoothed.append(-1) # add terminator
        flags.append(None) # add terminator
        for i in range(len(smoothed)):
            if (smoothed[i] >= threshold or smoothed[i] is None) and flags[i] == 0:
                if self.pairs[i] not in pairs_list:
                    pairs_list.append(self.pairs[i])
                pair_indices.append(i)
                if self.pairs[i][0] is not None: # 0 means chain B
                    n_res += 1 # increase counter only if there is a residue in chain B
                # check if there's a residue in chain B
                # that is below the threshold and makes a pair with this residue
                B_id = self.pairs[i][0] # 0 means chain B
                if B_id in comb[0]:
                    B_paired_id = comb[1][comb[0].index(B_id)]
                elif B_id in comb[1]:
                    B_paired_id = comb[0][comb[1].index(B_id)]
                else:
                    B_paired_id = None
                if B_paired_id is not None:
                    try:
                        where_is_B_paired = zip(*self.pairs)[0].index(B_paired_id)
                        if flags[where_is_B_paired] == 0 and\
                        smoothed[where_is_B_paired] < threshold:
                            paired_pair = self.pairs[where_is_B_paired]
                            if paired_pair not in pairs_list:
                                pairs_list.append(paired_pair)
                                pair_indices.append(where_is_B_paired)
                                n_res += 1
                            if paired_pair in self.frozen_pairs:
                                self.frozen_pairs.remove(paired_pair)
                    except ValueError:
                        print "WARNING: residue %s not found in chain B."\
                        % B_paired_id

            elif (smoothed[i] < threshold and smoothed[i] is not None or flags[i] == -1)\
            and n_res >= MIN_LENGTH:
                # check if there is at least one residue in the first chain
                #if len(chainA_residues) > 0:
                chainA_residues = [p[1] for p in pairs_list if p[1] is not None]
                chainB_residues = [p[0] for p in pairs_list if p[0] is not None]
                if len(chainA_residues) >= MIN_LENGTH and\
                   len(chainB_residues) >= MIN_LENGTH:
                    frags.append(pairs_list)
                    for ind in pair_indices:
                        flags[ind] = frag_num
                    frag_num += 1
                pairs_list = []
                pair_indices = []
                n_res = 0
            elif (smoothed[i] < threshold and smoothed[i] is not None or flags[i] == -1)\
            and n_res < MIN_LENGTH:
                n_res = 0
                pairs_list = []
                pair_indices = []

        # Once again incorporate "lone" residues into fragments
        # if they make pairs with residues from those fragments
        for i in range(len(flags)):
            if flags[i] == 0:
                B_id = self.pairs[i][0] # 0 means chain B
                if B_id in comb[0]:
                    B_paired_id = comb[1][comb[0].index(B_id)]
                elif B_id in comb[1]:
                    B_paired_id = comb[0][comb[1].index(B_id)]
                else:
                    B_paired_id = None
                if B_paired_id is not None:
                    try:
                        where_is_B_paired = zip(*self.pairs)[0].index(B_paired_id)
                        frag_num = flags[where_is_B_paired]
                        if frag_num > 0:
                            flags[i] = frag_num
                            lone_pair = self.pairs[i]
                            if lone_pair not in frags[frag_num - 1]:
                                frags[frag_num - 1].append(lone_pair)
                    except ValueError:
                        print "WARNING: residue %s not found in chain B."\
                        % B_paired_id

        # Incorporate short "islands" of residues below threshold
        # into neighboring fragments

        n_res = 0
        ind_list = []
        for i in range(len(flags)):
            if flags[i] == 0:
                # increase counter only if there's a residue in chain B
                if self.pairs[i][0] is not None:
                    n_res += 1
                ind_list.append(i)
            elif flags[i] == -1:

                n_res += 1
            else:
                if ind_list != [] and n_res < MIN_FROZEN_LENGTH:
                    # find fragment to merge the residues to
                    min_ind = min(ind_list)
                    if min_ind == 0:
                        frag_num = flags[i]
                    else:
                        frag_num = flags[min_ind - 1]
                    for ind in ind_list:
                        flags[ind] = frag_num
                        if frag_num > 0:
                            frags[frag_num - 1].append(self.pairs[ind])
                n_res = 0
                ind_list = []

        self.pairs.pop() # remove terminator
        smoothed.pop()  # remove terminator
        flags.pop() # remove terminator

        # Freeze fragments
        self.frozen_pairs = [self.pairs[ind] for ind in range(len(flags)) if flags[ind] < 1]

        return frags

    def find_fragments_with_clarnet(self, pairs):
        from Clarnet import clarnet, pdb_test
        if len(pairs) == 0:
            return []
        chain_B_ids = zip(*pairs)[0]
        ex = Extres()
        ex.extract_residues(self.pdb_data[1], self.model2,
                            self.chain2, chain_B_ids, False)
        tmpfile = tempfile.NamedTemporaryFile(prefix="supernalign", delete=False)
        tmpfile.write(ex.chain_fragment)
        tmpfile.close()
        graph_fname = os.path.join(TEMP_DIR, "clarnet.json")
        graph_file = clarnet.run_clarna(tmpfile.name, graph_file=graph_fname)
        contact_net = clarnet.load_contact_net_from_json(graph_file)
        filtered_net = clarnet.set_clarna_treshold(contact_net)
        m = pdb_test.clean_pdb(tmpfile.name)
        chains_list = pdb_test.find_chain_gaps(m[1])
        clarnet.add_covalent_contacts(filtered_net, chains_list)
        clarnet.assign_myscores(filtered_net)
        tmp_basename = os.path.basename(tmpfile.name)
        clarnet.run_mcl(filtered_net, 1.3, tmp_basename, TEMP_DIR)
        clusters = clarnet.get_clusters(TEMP_DIR, tmp_basename)
        fragments = []
        clusters = sort_clusters(clusters)

        for cluster in clusters:
            ids = [int(i[1:]) for i in cluster]
            frag = []
            pairs_0 = [pair[0] for pair in pairs]
            not_in_ids = [id for id in ids if id not in pairs_0]
            frag = [pair for pair in pairs if pair[0] in ids]

            if len(frag) > 0:
                frag_chainA = zip(*frag)[1] # check chain A
                chainA_not_None = [i for i in frag_chainA if i is not None]
                if len(chainA_not_None) > 0: # if it has at least one residue...
                    fragments.append(frag)   # ...append the fragment!

        temp = os.path.join(TEMP_DIR, tmp_basename + ".*")
        temp_files = glob.glob(temp)
        temp = os.path.join(os.path.realpath(os.curdir), "out.%s.mci.*" % tmp_basename)
        temp_files += glob.glob(temp)
        temp_files.append(tmpfile.name)
        temp_files.append(graph_fname)
        temp_files.append(m[0])
        for fname in temp_files:
            try:
                os.remove(fname)
            except:
                pass
        return fragments

    def sort_fragments(self, frags):
        """
        Sorts the fragments in clarnet-like way 
        """
        fake_chain = [["B"+str(a) for a in zip(*x)[0] if a != None] for x in frags]
        sorted = sort_clusters(fake_chain)
        fragments = []

        for cluster in sorted:
            ids = [int(i[1:]) for i in cluster]
            frag = []

            frag = [pair for pair in self.pairs if pair[0] in ids]

            if len(frag) > 0:
                frag_chainA = zip(*frag)[1] # check chain A
                chainA_not_None = [i for i in frag_chainA if i is not None]
                if len(chainA_not_None) > 0: # if it has at least one residue...
                    fragments.append(frag)   # ...append the fragment!

        return fragments

    def make_fragments(self, frags):
        """
        Yields Fragment instances for each pair list from frags.
        """

        list_of_fragments = []

        for f in frags:
            f.sort(cmp = sorting_function)
            add_frag = self.extract_fragment(f)
            list_of_fragments.append(add_frag)
        return list_of_fragments

    def pull_fragment(self, frag_data):
        """
        Pulls residues from pdb data.
        """

        ex_a = Extres()
        ex_b = Extres()
        res_b, res_a = zip(*frag_data)
        ex_a.pull_residues(self.pdb_data[0], self.model1, self.chain1, res_a, False)
        ex_b.pull_residues(self.pdb_data[1], self.model2, self.chain2, res_b, False)
        pulled = [ex_a.pulled_fragment, ex_b.pulled_fragment]

        return pulled

    def update_2d(self, frag_processed, frag_unprocessed):
        """
        Improved version of update(). If we take into account 2d structure,
        it is necessary to use this function instead of the old one.
        """
        to_remove = frag_unprocessed

        not_removed_ids = [i for i, j in enumerate(self.pairs) if j not in to_remove]
        new_pairs = [i for i in self.pairs if i not in to_remove] #remove old pairs
        new_sequences = [self.sequences[i] for i in not_removed_ids]
        new_unsmoothed = [self.unsmoothed[i] for i in not_removed_ids]

        new_pairs += frag_processed.pairs #add processed fragment data
        new_sequences += frag_processed.sequences
        new_unsmoothed += frag_processed.distances

        A, B = zip(*self.pairs)
        A = [i for i in sorted(A) if i is not None]
        B = [i for i in sorted(B) if i is not None]

        sorted_pairs = deepcopy(new_pairs)
        sorted_pairs.sort(cmp = sorting_function)
        order = [new_pairs.index(p) for p in sorted_pairs]
        sorted_seqs = [new_sequences[ind] for ind in order]
        sorted_uns = [new_unsmoothed[ind] for ind in order]

        #TODO: Should it be removed?
        missing = [i for i in new_pairs if i not in sorted_pairs]
        missing_ids = [i for i, j in enumerate(new_pairs) if j not in sorted_pairs]
        sorted_pairs += sorted(missing)
        sorted_seqs += [new_sequences[i] for i in missing_ids]
        sorted_uns += [new_unsmoothed[i] for i in missing_ids]

        self.pairs = sorted_pairs
        self.sequences = sorted_seqs
        self.unsmoothed = sorted_uns
        self.distances = self.smooth_distances(self.unsmoothed)

    def sort_pdb_data(self):
        """
        Sorts final pdb data.
        """

        def res_sorting(residue):
            try:
                res_id = residue[22:26]
                last_char = res_id[-1]
                if last_char >= "A" and last_char <= "z":
                    res_num = int(res_id[:-1]) + ord(last_char) / 100.0
                    return res_num
                return int(res_id)
            except:
                return None

        for i in range(len(self.pdb_data)):
            splitted = self.pdb_data[i].splitlines()
            splitted.sort(key = res_sorting)
            self.pdb_data[i] = '\n'.join(splitted)
            self.pdb_data[i] += '\nTER\n'

    def remove_structure_terminators(self):
        """
        Removes extra 'TER' and 'END' from pdb file.
        """

        for i in range(len(self.pdb_data)):
            temp = []
            temp2 = []
            temp = self.pdb_data[i].splitlines()
            for j in range(len(temp)):
                if temp[j] == 'TER' or temp[j] == 'END':
                    pass
                else:
                    temp2.append(temp[j])

            temp2 = '\n'.join(temp2)
            self.pdb_data[i] = temp2

    def superimpose_frozen(self, first_frozen):
        """
        Superimposes fragments frozen in the first iteration.
        """

        mean_dist_frozen = calc_mean_dist(self.distances)
        pdb_data_frozen = self.pdb_data[:]

        if first_frozen:

            first_frozen_long = [x for x in first_frozen if len(x) >= 8]
            make_frozen = self.make_fragments(first_frozen_long)
            for j in range(len(first_frozen_long)):
                self.update_2d(make_frozen[j], first_frozen_long[j])
                self.pdb_data = self.pull_fragment(first_frozen_long[j])
                self.pdb_data[0] += make_frozen[j].pdb_data[0]
                self.pdb_data[1] += make_frozen[j].pdb_data[1]

        mean_dist_unfrozen = calc_mean_dist(self.distances)

        if mean_dist_unfrozen > mean_dist_frozen:
            self.pdb_data = pdb_data_frozen[:]

    def process(self, list_of_combined):
        """
        Iteratively processes the input structures.
        The processing ends when either the entire structures are frozen
        or nothing was added to frozen_residues in the last iteration.
        """

        prev_found = []
        first_frozen = []
        iter_num = 0
        MAX_ITER_NUM = 50

        if args.debug:
            try:
                import glob
                for f in glob.glob("iteration_*.log"):
                    os.remove(f)
                for f in glob.glob("iteration_*.pdb"):
                    os.remove(f)
            except:
                pass
            with open("iteration_0.log", "wb") as f:
                print >> f, "*** Distances:"
                for i in range(len(self.distances)):
                    t = [self.pairs[i][1], self.sequences[i][1],
                         self.pairs[i][0], self.sequences[i][0],
                         self.unsmoothed[i], self.distances[i]]
                    t = [str(a) for a in t]
                    print >> f, "\t".join(t)

        while True:
            debug_info("Iteration %s:\n" % str(iter_num + 1))

            if USE_CLARNET and iter_num == 0:
                debug_info("Running Clarnet... ")
                found = self.find_fragments_with_clarnet(self.pairs)
                debug_info("%s total fragments found.\n" % str(len(found)))

            else:
                found = self.find_fragments(list_of_combined)
                found_uncomb = deepcopy(found)
                debug_info("%s fragment(s) found" % len(found_uncomb))
                if len(found) > 1 and list_of_combined:
                    found_combined = self.join_combined_fragments(found, list_of_combined)
                    found = found_combined
                if len(found)!=len(found_uncomb):
                    debug_info(" => %s fragment(s) combined" % len(found))
                debug_info(".\n")

                found = self.join_adjacent_fragments(found)


            frag_lengths = [len(fr) for fr in found]
            prev_lengths = [len(fr) for fr in prev_found]

            if found == [] or (set(sum(found, [])) == set(sum(prev_found, []))\
                and frag_lengths == prev_lengths) or iter_num >= MAX_ITER_NUM:
                break

            prev_found = []
            fragments_list = self.make_fragments(found)

            iter_num += 1

            if args.debug:
                frag_dir = "iteration_%s_fragments" % (iter_num,)
                if os.path.isdir(frag_dir):
                    shutil.rmtree(frag_dir)

                os.mkdir(frag_dir)

                for i,f in enumerate(found):
                    b_res = zip(*f)[0]
                    ex_b = Extres()
                    ex_b.extract_residues(self.pdb_data[1], self.model2, self.chain2, b_res, False)
                    fragment_pdb = open(os.path.join(frag_dir, "fragment_%d.pdb" % (i,)), "w")
                    fragment_pdb.write(ex_b.chain_fragment)
                    fragment_pdb.close()

                if self.frozen_pairs:
                    frozen_residues = zip(*self.frozen_pairs)[0]
                    ex_b_frozen = Extres()
                    ex_b_frozen.extract_residues(self.pdb_data[1], self.model2, self.chain2, frozen_residues, False)
                    frozen_pdb = open(os.path.join(frag_dir, "frozen_%d_iteration.pdb" % (iter_num,)), "w")
                    frozen_pdb.write(ex_b_frozen.chain_fragment)
                    frozen_pdb.close()

            for j in range(len(found)):
                if len(found[j]) >= 3:
                    self.update_2d(fragments_list[j], found[j]) #update pairs and distances
                    self.pdb_data = self.pull_fragment(found[j])

                    self.pdb_data[0] += fragments_list[j].pdb_data[0]
                    self.pdb_data[1] += fragments_list[j].pdb_data[1]
                    prev_found.append(fragments_list[j].pairs)

            if args.debug:
                self.save_output("iteration_%s" % iter_num, save_alignment=False,
                                 chainB_only=True)
                with open("iteration_%s.log" % iter_num, "wb") as f:
                    print >> f, "*** Distances:"
                    for i in range(len(self.distances)):
                        pair = self.pairs[i]
                        t = [pair[1], self.sequences[i][1],
                             pair[0], self.sequences[i][0],
                             self.unsmoothed[i], self.distances[i]]
                        t = [str(a) for a in t]
                        if pair in self.frozen_pairs:
                            t.append("*")
                        print >> f, "\t".join(t)
                    print >> f, "\n*** Fragments:"
                    for frag in found:
                        print >> f, "\n", frag
                    print >> f, "\n*** Frozen:"
                    print >> f, self.frozen_pairs

        self.remove_structure_terminators()  #remove TER and END
        self.sort_pdb_data() #sort pdb files by residue number
        self.renumber_atoms()

    def join_combined_fragments(self, fragments, combined):
        """
        Joins fragments found by find_combined_fragments()
        """

        for comb in combined:
            glued = []
            for i in range(len(fragments)):
                a = zip(*fragments[i])[0] # 0 because 2nd chain residues are first in pairs
                if int(comb[1]) in a:
                    if int(comb[2]) in a:
                        break
                    else:
                        for y in range(len(fragments)):
                            if y == i: continue #if two combined residues are already in the same fragment
                            a2 = zip(*fragments[y])[0]
                            if int(comb[2]) in a2:
                                glued = fragments[i] + fragments[y]
                                to_remove = {i, y}
                                fragments = [i for j, i in enumerate(fragments)\
                                               if j not in to_remove]
                                fragments.append(glued)
                                break
                        else: continue
                        break
                else: continue
        return fragments

    def get_sequence_alignment(self):
        """
        Generates and returns the output sequence alignment.
        """

        if self.sequence_alignment is None:
            seq_aln = Pdb3align()
            from math import sqrt
            seq_aln.THRESHOLD = 12.0 * sqrt(2) #change dist threshold for final alignment
            seq_aln.align(self.pdb_data[0], self.model1, self.chain1,\
                          self.pdb_data[1], self.model2, self.chain2, False)
            self.sequence_alignment = [seq_aln.basesA, seq_aln.basesB]
        return self.sequence_alignment

    def renumber_atoms(self):
        """
        Renumbers atoms in pdb files.
        """

        for i in range(len(self.pdb_data)):
            counter = 1
            atom_list = []
            renumbered = []
            atom_list = self.pdb_data[i].splitlines()
            for line in atom_list:
                if line[0:6] in ('ATOM  ', 'HETATM'):
                    renumbered.append("%s%5s%s" % (line[0:6], counter, line[11:]))
                    counter += 1
                else:
                    renumbered.append(line)
            self.pdb_data[i] = '\n'.join(renumbered)

    def save_output(self, output_name, save_alignment=True, chainB_only=False):
        """
        Saves output pdb file with aligned structures and txt file with sequence alignment.
        """
        pdb_file = open(output_name + '.pdb', 'w')
        if not chainB_only:
            pdb_file.write(self.pdb_data[0])
        pdb_file.write('\n')
        pdb_file.write(self.pdb_data[1])
        pdb_file.close()

        if save_alignment:
            alignment = open(output_name + '.aln','w')
            alignment.write('>chainA\n')
            alignment.write(self.sequence_alignment[0])
            alignment.write('\n')
            alignment.write('>chainB\n')
            alignment.write(self.sequence_alignment[1])
            alignment.close()


def find_combined_fragments(pdb_data_B):
    """
    Runs X3DNA for every pdb file to find combined residues.
    Returns the list of tuples in following format:
    <chain id> <reside id> <residue id> <chain id>
    i.e.
    ('A','1','34','A')
    """

    combined = []
    reg_exp = re.compile('>([-A-Z0-9]):[.]*([0-9]+)_:[^:]*:[.]*([0-9]+)_:([-A-Z0-9])<')
    os.environ["X3DNA"] = X3DNA_PATH
    os.environ["PATH"] = os.path.join(X3DNA_PATH, "bin") + ":" + os.environ["PATH"]
    X3DNA_OUT = os.path.join(TEMP_DIR, "x3dna.out")

    if os.name == 'nt':
        os.system("find_pair %s %s > NUL 2>&1 " % (pdb_data_B, X3DNA_OUT))
    elif os.name == 'posix':
        os.system("find_pair %s %s > /dev/null 2>&1" % (pdb_data_B, X3DNA_OUT))
    else:
        os.system("find_pair %s %s" % (pdb_data_B, X3DNA_OUT))

    fnames = ['bp_order.dat', 'col_chains.scr', 'col_helices.scr',
              'ref_frames.dat', 'bestpairs.pdb', 'hel_regions.pdb']

    for fn in fnames:
        if os.path.exists(fn):
            os.remove(fn)

    try:
        fh = open(X3DNA_OUT).read()
        combined = reg_exp.findall(fh)
    except:
        combined = []

    if combined == []:
        print "WARNING: Secondary structure not determined!"

    return combined

def calc_mean_dist(distances):
    not_nones = [x for x in distances if x is not None]
    sum = numpy.mean(not_nones)
    return sum

def calc_med_dist(distances):
    not_nones = [x for x in distances if x is not None]
    med = numpy.median(not_nones)
    return med

def check_dist_ratio(distances):
    """
    Checks whether we should proceed or change preliminary superposition parameters.
    """

    ratio_thresh = 1.5
    below_threshold = [x for x in distances if x < ratio_thresh and x is not None]
    below_threshold_len = len(below_threshold)
    all_dists = len(distances)

    dist_ratio = float(below_threshold_len)/float(all_dists)
    return dist_ratio

def clean_structure(pdb_file, chain_id, prefix=""):
    """
    Cleans, unmodifies and renumbers structure.
    Returns the output file name.
    """
    try:
        import moderna
    except ImportError:
        print "ModeRNA not installed, can't clean structure."
        return pdb_file
    struct = moderna.load_model(pdb_file, chain_id)
    moderna.clean_structure(struct)
    moderna.remove_all_modifications(struct)
    moderna.renumber_chain(struct, "1")
    split_name = os.path.splitext(os.path.basename(pdb_file))
    new_name = prefix + split_name[0] + "_" + chain_id + split_name[1]
    new_pdb_file = os.path.join(TEMP_DIR, new_name)
    struct.write_pdb_file(new_pdb_file)
    return new_pdb_file


if __name__ == "__main__":
    if len(sys.argv) < 6:
        exit("""SupeRNAlign %s by Pawel Piatkowski and Jagoda Jablonska
http://genesilico.pl/
USAGE:
    supernalign.py <pdb_file1> <model1_id> <chain1_id> [<pdb_file2>] <model2_id> <chain2_id> [-o <output_name>] [-p] [-c]""" % __VERSION__)
    else:
        DEFAULT_OUTPUT = "supernalign_output"
        parser = argparse.ArgumentParser()
        parser.add_argument('pdb_data_A')
        parser.add_argument('model1_id')
        parser.add_argument('chain1_id')
        parser.add_argument('pdb_data_B', nargs = '?')
        parser.add_argument('model2_id')
        parser.add_argument('chain2_id')
        parser.add_argument('-o', '--output', action = 'store',
                            help = 'Output files name', dest = 'output',
                            default = DEFAULT_OUTPUT)
        parser.add_argument('-p', '--preliminary', action = 'store_true',
                        help = 'Input structures are pre-superimposed')
        parser.add_argument('-d', '--debug', action = 'store_true')
        parser.add_argument('-c', '--clean', action = 'store_true',
                        help = 'Clean input structures')
        parser.add_argument('-u', '--use-clarnet', action = 'store',
                        choices = ('yes', 'no', 'true', 'false'),
                        help = 'Prealign structures')
        parser.add_argument('-a', '--prealign', action = 'store_true',
                        help = 'Prealign structures')
        parser.add_argument('-b', '--chainB-only', action = 'store_true',
                        help = 'Write only the aligned structure to .pdb file')

        args = parser.parse_args()

        # for custom output file name, create a custom temporary directory
        if args.output != DEFAULT_OUTPUT:
            TEMP_DIR = os.path.join(TEMP_DIR, args.output)
            if not os.path.exists(TEMP_DIR):
                os.makedirs(TEMP_DIR)

        if args.use_clarnet in ('yes', 'true'):
            USE_CLARNET = True
            args.clean = True
        elif args.use_clarnet in ('no', 'false'):
            USE_CLARNET = False

        if args.prealign:
            from prealign import Prealign
            args.clean = True
            args.preliminary = True
            USE_CLARNET = False # we don't need another clustering

        if args.clean:
            if not args.pdb_data_B:
                args.pdb_data_B = args.pdb_data_A
            args.pdb_data_A = clean_structure(args.pdb_data_A,
                              args.chain1_id, "chainA_")
            args.pdb_data_B = clean_structure(args.pdb_data_B,
                              args.chain2_id, "chainB_")

        if not args.pdb_data_B:
            pdb_data_A = []
            pdb_data_B = []
            pdb_str = open(args.pdb_data_A, 'r').readlines()

            for line in pdb_str:
                line_split = line.split()
                if len(line_split) < 5:
                    pass
                else:
                    if (line_split[0] == 'ATOM' or line_split[0] == 'HETATM')\
                    and line_split[4] == args.chain1_id:
                        pdb_data_A.append(line)
                    elif (line_split[0] == 'ATOM' or line_split[0] == 'HETATM')\
                    and line_split[4] == args.chain2_id:
                        pdb_data_B.append(line)

            pdb_data_A = ''.join(pdb_data_A)
            pdb_data_B = ''.join(pdb_data_B)

            print '2D structure processing...'
            all_combined = find_combined_fragments(args.pdb_data_A)
            combined = [i for i in all_combined if i[0] == args.chain2_id\
                                               and i[3] == args.chain2_id]

        else:
            pdb_data_A = open(args.pdb_data_A, 'r').read()
            original_chain_id = args.chain2_id

            if args.prealign:
                # Even if there was only one input file,
                # cleaning has already split it into two
                print 'Prealigning...'
                prealigner = Prealign(args.pdb_data_A, args.pdb_data_B)
                pdb_data_B = prealigner.get_aligned_structure()
                args.chain2_id = "A"
            else:
                pdb_data_B = open(args.pdb_data_B, 'r').read()

            print '2D structure processing...'
            combined = find_combined_fragments(args.pdb_data_B)
            combined = [i for i in combined if i[0] == original_chain_id\
                                           and i[3] == original_chain_id]

    if not args.preliminary:
        print 'Preliminary superposition...'
        sup = Superposition(pdb_data_A, pdb_data_B, args.chain1_id,\
              args.chain2_id, args.model1_id, args.model2_id, True)
    else:
        sup = Superposition(pdb_data_A, pdb_data_B, args.chain1_id,\
              args.chain2_id, args.model1_id, args.model2_id, False)

    #import doctest
    #doctest.testmod(globs={"s": sup}, optionflags=doctest.ELLIPSIS)
    print 'Processing...'

    if args.debug:
        pdb_file = open('supernalign_preliminary.pdb', 'w')
        pdb_file.write(sup.pdb_data[0])
        pdb_file.write('\n')
        pdb_file.write(sup.pdb_data[1])
        pdb_file.close()

    sup.process(combined)
    aln = sup.get_sequence_alignment()

    print '>Chain A'
    print aln[0]
    print '>Chain B'
    print aln[1]

    print 'Saving output...'
    sup.save_output(args.output, chainB_only=args.chainB_only)
    print 'Wrote output to ' + args.output + '.pdb and ' + args.output + '.aln'

    if args.prealign:
        try:
            os.remove(prealigner.aligned_filename)
        except OSError:
            pass

    if args.output != DEFAULT_OUTPUT:
        #remove temporary directory and its contents
        shutil.rmtree(TEMP_DIR, ignore_errors=True)
