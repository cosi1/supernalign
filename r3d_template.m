cd {{r3d_dir}};
warning('off','all');
addpath(genpath(pwd));
Query.Type = 'local';
Query.Name = '{{name1}}_{{name2}}';
Query.UploadName1 = '{{name1}}.pdb';
Query.UploadName2 = '{{name2}}.pdb';
[a1,a2] = R3DAlign('uploaded', {'{{chain1}}'}, {'all'}, 'uploaded', {'{{chain2}}'}, {'all'}, {{d}}, {{p}}, {{beta}}, {{q}}, Query);

