#!/usr/bin/env python
from Bio.PDB import *
import sys

# merge_models.py version 2 <2014/01/30>
# changes: now accepts empty second or fourth model
# now you can customize chain ids

class PDBParsingError(Exception):
    pass


def merge_models(in_file, chain1_id, chain2_id):
            
    parser = PDBParser()
    try:
        s_in = parser.get_structure("str1", in_file)
    except ValueError as e:
        raise PDBParsingError("Something's wrong with your .pdb file (%s)" % e)
    m1, m2, m3, m4 = s_in.child_list
    c1 = m1.child_list[0]
    try:
        c2 = m2.child_list[0]
    except:
        c2 = []
    c3 = m3.child_list[0]
    try:
        c4 = m4.child_list[0]
    except:
        c4 = []

    for r in c2: c1.add(r)
    c1_sorted = sorted(c1, key=lambda x: x.id[1])
    c1_out = Chain.Chain('A')
    for r in c1_sorted: c1_out.add(r)

    for r in c4: c3.add(r)
    c3_sorted = sorted(c3, key=lambda x: x.id[1])
    c2_out = Chain.Chain('B')
    for r in c3_sorted: c2_out.add(r)

    m_out = Model.Model(1)
    m_out.add(c1_out)
    m_out.add(c2_out)
    s_out = Structure.Structure("str2")
    s_out.add(m_out)
        
    return s_out
        

if __name__=="__main__":
    
    if len(sys.argv)<3:
       exit("USAGE: merge_models.py <pdb_file_in> <outname> <chain1_id> <chain2_id>")
    elif len(sys.argv) == 3:
       in_file, out_file= sys.argv[1:]
       chain1_id = "A"
       chain2_id = "B"
    elif len(sys.argv) == 5:
       in_file, out_file, chain1_id, chain2_id = sys.argv[1:]

    s_out = merge_models(in_file, chain1_id, chain2_id)
    io = PDBIO()
    io.set_structure(s_out)
    io.save(out_file)

