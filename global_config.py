import os
from ConfigParser import ConfigParser

CUR_DIR = os.getcwd()
SUPERALN_DIR = os.path.dirname(os.path.realpath(__file__))

CONFIG_FILE = os.path.join(SUPERALN_DIR, "config_file.cfg")
Config = ConfigParser()
Config.read(CONFIG_FILE)

X3DNA_PATH = Config.get('CONFIGURATION FILE', 'X3DNA_PATH')
SUPERIMPOSER_PATH = Config.get('CONFIGURATION FILE', 'SUPERIMPOSER_PATH')
USE_SMATCH = Config.getboolean('CONFIGURATION FILE', 'USE_SMATCH')
if USE_SMATCH:
    SMATCH_PATH = Config.get('CONFIGURATION FILE', 'SMATCH_PATH')
USE_CLARNET = Config.getboolean('CONFIGURATION FILE', 'USE_CLARNET')

TEMP_DIR = os.path.join(SUPERALN_DIR, 'temp')
if not os.path.exists(TEMP_DIR):
    os.makedirs(TEMP_DIR)

