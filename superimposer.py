#!/usr/bin/env python

from StringIO import StringIO
from Bio.PDB import PDBParser, Model, Structure, PDBIO
import os, glob, sys
import hashlib, random
from global_config import SUPERALN_DIR, SUPERIMPOSER_PATH, TEMP_DIR


class NoChainException(Exception):
    pass


class SuperimposerException(Exception):
    pass


class Superimposer(object):
    """
    Generic class for superimposing fragments
    """
    
    def __init__(self, pdb_data, temp_dir = None):
        self.pdb_data_A = pdb_data[0]
        self.pdb_data_B = pdb_data[1]
        self.pdb_data_out = []
        self.first_res_id = None # ID of the first residue from chain A
        if temp_dir is None:
            self.TEMP_DIR = TEMP_DIR
        else:
            self.TEMP_DIR = temp_dir
       
    def pdb_temp_files(self):
        """
        Creates temporary pdb files from Fragment objects to prepare input
        Returns hashed IDs and full paths to both files
        """
        
        name1 = hashlib.sha256(str(random.randrange(0, 1e20))).hexdigest()
        name2 = hashlib.sha256(str(random.randrange(0, 1e20))).hexdigest()
        
        pdb_name1 = os.path.join(self.TEMP_DIR, name1 + '.pdb')
        pdb1 = open(pdb_name1, 'wb')
        pdb1.write(self.pdb_data_A)
        pdb1.close()
        pdb_name2 = os.path.join(self.TEMP_DIR, name2 + '.pdb')
        pdb2 = open(pdb_name2, 'wb')
        pdb2.write(self.pdb_data_B)
        pdb2.close()
        
        return name1, name2, pdb_name1, pdb_name2

    def get_chain_length(self, name, chain_id):
        """
        Returns length (in residues) of a chain from a .pdb file.
        """
        parser = PDBParser()
        filename = os.path.join(self.TEMP_DIR, name + ".pdb")
        s = parser.get_structure("frag", filename)
        model = s[0]
        try:
            chain = model[chain_id]
        except KeyError, e:
            raise NoChainException('There is no chain with id %s' % str(e))

        return len(chain)

    def get_dists(self, ref_file, is_struct):
        """
        Returns list of coordinates for P (or C4') atom from the first residue.
        """
        if is_struct is False:
            ref_pdb = StringIO(ref_file)
            p = PDBParser()
            ref_struct = p.get_structure(1, ref_pdb)
        else: 
            ref_struct = ref_file
        ref_model = ref_struct.child_list[0]
        ref_chain = ref_model.child_list[0]

        # in the first phase, take the first residue and atom from the list...
        if self.first_res_id is None:
            ref_res = list(ref_chain)[0]
            self.first_res_id = ref_res.id[1]
            try:
                atom = ref_res["P"]
                self.atom_id = "P"
            except KeyError:
                atom = ref_res["C4'"]
                self.atom_id = "C4'"
        # ...in the second phase, use the ID of that residue (and atom)
        else:
            ref_res = ref_chain[self.first_res_id]
            atom = ref_res[self.atom_id]
        coords = atom.get_coord()

        return coords

    def update_coords(self, coords_start, coords_imposed, structures):
        """
        Updates coordinates.
        """
        dx = coords_start[0] - coords_imposed[0]
        dy = coords_start[1] - coords_imposed[1]
        dz = coords_start[2] - coords_imposed[2]

        for struct in structures:
            #iterate over atoms and update coordinates based on deltas
            for atom in struct.get_atoms():
                atom.coord[0] += dx
                atom.coord[1] += dy
                atom.coord[2] += dz

        return structures 

    def impose(self, model_id1, chain_id1, model_id2, chain_id2):
        """
        Imposes Fragments 
        """

        pass

