#!/usr/bin/env python

from StringIO import StringIO
from Bio.PDB import PDBParser, Model, Structure, PDBIO
from global_config import SUPERALN_DIR, SUPERIMPOSER_PATH, X3DNA_PATH
from superimposer import Superimposer, SuperimposerException
import re, os, glob, sys, shutil, subprocess
import xml.etree.ElementTree as et
import numpy as np

TEMPLATE = os.path.join(SUPERALN_DIR, 'setter_template.xml')
SETTER_FILE = os.path.join(SUPERIMPOSER_PATH, "setter")


class SETTER_Superimposer(Superimposer):
    """
    Superimposes Fragments using R3DAlign 
    """

    def generate_xml(self, name1, chain1, name2, chain2):
        """
        Generates input file for SETTER
        """

        with open(TEMPLATE, "rb") as f:
            tmpl = "".join(f.readlines())

        repl_table = (("{{name1}}", name1),
                      ("{{chain1}}", chain1),
                      ("{{name2}}", name2),
                      ("{{chain2}}", chain2))
        for tag, value in repl_table:
            tmpl = tmpl.replace(tag, value)
        
        with open(self.IN_FILE, "wb") as f:
            f.write(tmpl)

    def run_x3dna(self, input_file, output_file):
        reg_exp = re.compile('>([-A-Z0-9]):[.]*([0-9]+)_:[^:]*:[.]*([0-9]+)_:([-A-Z0-9])<')
        os.putenv("X3DNA", X3DNA_PATH)
        os.putenv("PATH", os.path.join(X3DNA_PATH, "bin") + ":" +
                          os.environ["PATH"])

        X3DNA_COMMAND = "find_pair %s %s" % (input_file, output_file)
        if os.name == 'nt':
            os.system("%s > NUL 2>&1 " % X3DNA_COMMAND)
        elif os.name == 'posix':
            os.system("%s > /dev/null 2>&1" % X3DNA_COMMAND)
        else:
            os.system(X3DNA_COMMAND)

        fnames = ['bp_order.dat', 'col_chains.scr', 'col_helices.scr',
                  'ref_frames.dat', 'bestpairs.pdb', 'hel_regions.pdb']
        for fn in fnames:
            if os.path.exists(fn):
                os.remove(fn)

    def impose(self, model_id1, chain_id1, model_id2, chain_id2):
        """
        Imposes Fragments 
        """

        self.IN_FILE = os.path.join(self.TEMP_DIR, 'setter_in.xml')
        self.OUT_FILE = os.path.join(self.TEMP_DIR, 'setter_out.xml')
        name1, name2, pdb1, pdb2 = self.pdb_temp_files()
        pdb1_noext = os.path.splitext(pdb1)[0]
        pdb2_noext = os.path.splitext(pdb2)[0]
        self.run_x3dna(pdb1, pdb1_noext + ".out")
        self.run_x3dna(pdb2, pdb2_noext + ".out")
        self.generate_xml(pdb1_noext, chain_id1, pdb2_noext, chain_id2)

        command = "%s -in %s -out %s" %\
            (SETTER_FILE, self.IN_FILE, self.OUT_FILE)

        nullDev = open(os.devnull, "w")
        errCode = subprocess.call(command, stdout = nullDev, stderr = nullDev, shell = True)

        if errCode != 17:
            raise SuperimposerException("""
SETTER couldn't be run. Please check SUPERIMPOSER_PATH in the config file.""")

        try:
            self.apply_rot_tran()
        except IOError:
            self.pdb_data_out = [self.pdb_data_A, self.pdb_data_B] 	    
            #print "SETTER: Fragment not aligned!" #XXX

        # remove redundant files from temp directory
        temp_files = glob.glob(os.path.join(self.TEMP_DIR, '*'))
        for file_ in temp_files:
            os.remove(file_)

    def apply_rot_tran(self):
        """
        Superimposes input structures by applying rotation/translation data
        from SETTER's output file
        """

        parser = PDBParser()
        structure1 = StringIO(self.pdb_data_A)
        s1 = parser.get_structure("str1", structure1)
        structure2 = StringIO(self.pdb_data_B)
        s2 = parser.get_structure("str2", structure2)
        c1 = s1.child_list[0].child_list[0]
        c2 = s2.child_list[0].child_list[0]

        data = et.parse(self.OUT_FILE)
        root = data.getroot()
        rot_data = root.find("result").find("rotation")
        rot = eval(rot_data.text)
        tran_data = root.find("result").find("translation")
        tran = eval(tran_data.text)

        for atom in c2.get_atoms():
            atom.set_coord(np.add(np.dot(np.array(rot),
                           np.array(atom.get_coord())), np.array(tran)))

        m1 = Model.Model(0, 0)
        c1.id = "A"
        m1.add(c1)
        s1 = Structure.Structure("out1")
        s1.add(m1)
        m2 = Model.Model(0, 0)
        c2.id = "B"
        m2.add(c2)
        s2 = Structure.Structure("out2")
        s2.add(m2)
        io = PDBIO()
        io.set_structure(s1)
        out_data1 = StringIO()
        io.save(out_data1)
        io.set_structure(s2)
        out_data2 = StringIO()
        io.save(out_data2)
        self.pdb_data_out = [out_data1.getvalue(), out_data2.getvalue()]
        

if __name__ == "__main__":
    if len(sys.argv) < 5:
        print "USAGE: setter_superimposer.py pdb_file1 chain1_id pdb_file2 chain2_id [output_file]"
        exit()
    if len(sys.argv) == 6:
        output_file = sys.argv.pop()
    else:
        output_file = "output.pdb"
    pdb_file1, chain1_id, pdb_file2, chain2_id = sys.argv[1:5]
    with open(pdb_file1, "rb") as f:
        pdb_dataA = f.read()
    with open(pdb_file2, "rb") as f:
        pdb_dataB = f.read()

    sup = SETTER_Superimposer([pdb_dataA, pdb_dataB])
    sup.impose(0, chain1_id, 0, chain2_id)
    with open(output_file, "wb") as f:
        f.write(sup.pdb_data_out[0])
        f.write(sup.pdb_data_out[1])

