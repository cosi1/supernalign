#!/bin/sh

# args:
# $1, $2 - input files (without extensions)
# $3 - path to X3DNA
# $4 - temporary directory
# $5 - path to ARTS
# $6 - path to SupeRNAlign

X3DNA_DIR=$3
X3DNA_BIN_DIR=$X3DNA_DIR"/bin"
TEMP_DIR=$4
ARTS_DIR=$5
SA_DIR=$6

export X3DNA=$X3DNA_DIR
export PATH=$X3DNA_BIN_DIR:$PATH

echo $0: Preprocessing $1.pdb...
cd $TEMP_DIR

find_pair $1".pdb" x3dna.out
$SA_DIR/arts_parse3.pl x3dna.out > $1".pairs"
find_pair $2".pdb" x3dna.out
$SA_DIR/arts_parse3.pl x3dna.out > $2".pairs"

$ARTS_DIR/arts $1".pdb" $2".pdb"
perl $ARTS_DIR/arts2pdb arts.out 1 .
mv "model1."$1".pdb" "arts_"$1".pdb"
mv "model2."$2".pdb" "arts_"$2".pdb"

