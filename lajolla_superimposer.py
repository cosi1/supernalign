#!/usr/bin/env python

from StringIO import StringIO
from Bio.PDB import PDBParser, Model, Structure, PDBIO
from shutil import rmtree
from operator import itemgetter
from global_config import SUPERALN_DIR, SUPERIMPOSER_PATH
from superimposer import Superimposer, SuperimposerException
import os, glob, sys, shutil, subprocess
import re

class LaJolla_Superimposer(Superimposer):
    """
    Superimposes Fragments using LaJolla
    """
    
    def impose(self, model_id1, chain_id1, model_id2, chain_id2):
        """
        Imposes Fragments 
        """

        coords1 = self.get_dists(self.pdb_data_A, False) #coords before superposition
        name1, name2, pdb1, pdb2 = self.pdb_temp_files()

        command = " ".join(("java", "-cp",
                            os.path.join(SUPERIMPOSER_PATH, "lajolla.jar"),
                            "RNA", "-q", pdb1, "-t", pdb2,
                            "-o", self.TEMP_DIR,
                            "-zn 4"))

        nullDev = open(os.devnull, "w")
        errCode = subprocess.call(command, stdout = nullDev, stderr = nullDev, shell = True)

        if errCode != 0:
            raise SuperimposerException("""
LaJolla couldn't be run. Please check SUPERIMPOSER_PATH in the config file.""")

        OUTPUT_DIR = "%s-model-%s-chain-%s" % (pdb1, model_id1, chain_id1)
        LAJOLLA_OUTPUT_A = os.path.join(OUTPUT_DIR, "q-%s.pdb_original.pdb" % name1)
        files_list = glob.glob(os.path.join(OUTPUT_DIR,
                     "t-%s.pdb-m-%s-c-%s-*.pdb" % (name2, model_id2, chain_id2)))
        if len(files_list)==0:
            self.pdb_data_out = [self.pdb_data_A, self.pdb_data_B] 	    
            #print "LaJolla: Fragment not aligned!" #XXX

        else:
            pattern = re.compile(r"t\-%s.pdb\-m\-\d+\-c\-([A-Z0-9]+)\-RS\-([\d.]+)\-" % name2)
            model_details = [re.findall(pattern, f) for f in files_list]
            which_file, chain_and_rmsd = min(enumerate(model_details),
                                             key=lambda x:itemgetter(1))
            LAJOLLA_OUTPUT_B = files_list[which_file]
            
            chain_out_id = chain_and_rmsd[0][0]

            list_of_structures = []
            parser = PDBParser()
            structure_out_A = parser.get_structure("lj01", LAJOLLA_OUTPUT_A)
            model_out_A = structure_out_A[0]

            chain_out_A = model_out_A[chain_id1]
            if chain_out_A.id != "A":
            	chain_out_A.id = "A"
            structure_out_B = parser.get_structure("lj02", LAJOLLA_OUTPUT_B)
            model_out_B = structure_out_B[0]
            chain_out_B = model_out_B[chain_out_id]
            if chain_out_B.id != "B":
            	chain_out_B.id = "B"
            list_of_structures = (structure_out_A, structure_out_B)
        
            '''
            coords2 = self.get_dists(list_of_structures[0], True) #coords after superposition
            structs_coords_edited = self.update_coords(coords1, coords2, list_of_structures)

            for struct in structs_coords_edited:
            '''
            for struct in list_of_structures:
                pio = PDBIO()
                fragment_file = StringIO()
                pio.set_structure(struct)
                pio.save(fragment_file)
                self.pdb_data_out.append(fragment_file.getvalue())

        # remove temporary files
        try:
            temp_files = [pdb1, pdb2]
            for tf in temp_files:
                if os.path.exists(tf):
                    os.remove(tf)
            rmtree(OUTPUT_DIR)
        except OSError:
            pass
            
