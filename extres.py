#!/usr/bin/env python
from Bio.PDB import PDBParser, Structure, Model, Chain, PDBIO
from sys import argv, stdout
from StringIO import StringIO

import warnings
warnings.simplefilter("ignore")

class Extres(object):
    
    def __init__(self):
        self.chain_fragment = ""
        self.pulled_fragment = ""
         
    def parse_input(self, in_filename, model, chain, dec):
        """
        Converts input structures to Bio.PDB objects.
        """
        if dec == False:
            in_filename = StringIO(in_filename)
        else:
            in_filename = in_filename
     
        p = PDBParser()
        s_in = p.get_structure(1, in_filename)
        m_in = model == "0" and s_in.child_list[0] or s_in[model]
        c_in = m_in[chain]
        return c_in

    def extract_residues(self, in_filename, model, chain, residues, dec):
        """
        Extracts fragment of given structure.
        """
        chain_in = self.parse_input(in_filename, model, chain, dec)
        structure_out = Structure.Structure(0)
        model_out = Model.Model(0)
        chain_out = Chain.Chain(chain)
        
        for r in chain_in.get_residues():
            r_id = r.get_id()[1]
            if r_id in residues and r_id is not None:
                chain_out.add(r)
        
        fragment_file = StringIO()
        model_out.add(chain_out)
        structure_out.add(model_out)
        pio = PDBIO()
        pio.set_structure(structure_out)
        pio.save(fragment_file)
        self.chain_fragment = fragment_file.getvalue()
        
    def pull_residues(self, in_filename, model, chain, residues, dec):
        """
        Permanently pulls out residues from given structure.
        """
        chain_in = self.parse_input(in_filename, model, chain, dec)
        structure_out = Structure.Structure(0)
        model_out = Model.Model(0)
        chain_out = Chain.Chain(chain)
        
        for r in chain_in.get_residues():
            r_id = r.get_id()[1]
            if r_id not in residues:
                chain_out.add(r)
                
        fragment_file = StringIO()
        model_out.add(chain_out)
        structure_out.add(model_out)
        pio = PDBIO()
        pio.set_structure(structure_out)
        pio.save(fragment_file)
        self.pulled_fragment = fragment_file.getvalue()
         
        
if __name__ == "__main__":
    if len(argv) < 5:
        exit("Usage: extres.py <input_filename> <model> <chain> <residues> [<output_filename>]\n\
      ex.: extres.py rna.pdb 0 B 7-10,15,20-30 rna_frag.pdb")
      
    if len(argv) > 5:
        out_filename = argv[5]
        f = open(out_filename, "wb")
    else:
        f = stdout
        
    in_filename = argv[1]
    model = argv[2]
    chain = argv[3]
    str_res = argv[4].split(",")
    residues = []
    
    for r in str_res:
        if "-" in r:
            r_split = r.split("-")
            residues.extend(range(int(r_split[0]), int(r_split[1]) + 1))
        else:
            residues.append(int(r))
            
    ex = Extres()
    ex.extract_residues(in_filename, model, chain, residues, True)
    f.write(ex.chain_fragment)
    f.close()
