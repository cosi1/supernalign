#!/usr/bin/env python

from merge_models import merge_models
from StringIO import StringIO
from Bio.PDB import PDBParser, Model, Structure, PDBIO
from global_config import SUPERALN_DIR, SUPERIMPOSER_PATH
from superimposer import Superimposer, SuperimposerException
import os, glob, sys, shutil, subprocess

TEMPLATE = os.path.join(SUPERALN_DIR, 'r3d_template.m')


class R3D_Superimposer(Superimposer):
    """
    Superimposes Fragments using R3DAlign 
    """
    
    def generate_r3d_script(self, name_1, name_2, chain_id1, chain_id2,\
                            chain1_len, chain2_len, output_name):
        """
        Generates input script to be run by Octave
        """

        d = "{0.4; 0.5; 0.5}"
        p = "{1; 3; 9}"
        beta = "{200; 70; 20}"
        q = "{'greedy'; 'greedy'; 'greedy'}"


        chain1 = chain_id1
        chain2 = chain_id2
        name1 = name_1
        name2 = name_2

        repl_table = (("{{r3d_dir}}", SUPERIMPOSER_PATH),
                  ("{{output_name}}", output_name),
                  ("{{name1}}", name1),
                  ("{{chain1}}", chain1),
                  ("{{name2}}", name2),
                  ("{{chain2}}", chain2),
                  ("{{d}}", d),
                  ("{{p}}", p),
                  ("{{beta}}", beta),
                  ("{{q}}", q))

        fh1 = open(TEMPLATE, "rb")
        tmpl = "".join(fh1.readlines())
        fh1.close()

        for tag, value in repl_table:
            tmpl = tmpl.replace(tag, value)
        
        fh2 = open(os.path.join(self.TEMP_DIR, 'r3d_script.m'), "wb")
        fh2.write(tmpl)
        fh2.close()

    def impose(self, model_id1, chain_id1, model_id2, chain_id2):
        """
        Imposes Fragments 
        """

        coord1 = self.get_dists(self.pdb_data_A, False) #coords before superposition
        name1, name2, pdb1, pdb2 = self.pdb_temp_files()
        chain1_len = self.get_chain_length(name1, chain_id1)
        chain2_len = self.get_chain_length(name2, chain_id2)
        output_name = "%s_%s" % (name1, name2)
        self.generate_r3d_script(name1, name2, chain_id1, chain_id2,\
                                 chain1_len, chain2_len, output_name)

        for f in [pdb1, pdb2]:
            os.system("cp %s %s" % (f, os.path.join(SUPERIMPOSER_PATH, 'PDBFiles')))

        command = "octave %s" % os.path.join(self.TEMP_DIR, "r3d_script.m")
        nullDev = open(os.devnull, "w")
        errCode = subprocess.call(command, stdout = nullDev, stderr = nullDev, shell = True)

        if errCode != 0:
            raise SuperimposerException("""
R3D Align couldn't be run. Please check SUPERIMPOSER_PATH in the config file.""")

        try:
            output_file = "%s.pdb" % output_name
            shutil.move(os.path.join(SUPERIMPOSER_PATH, output_file),
                        os.path.join(self.TEMP_DIR, output_file))
            merged_structure = merge_models(os.path.join(self.TEMP_DIR, output_file), 
                                            chain_id1, chain_id2)
        except IOError:
            self.pdb_data_out = [self.pdb_data_A, self.pdb_data_B] 	    
            #print "R3D Align: Fragment not aligned!" #XXX
        else:
            list_of_structures = []
        
            for chain in merged_structure.get_chains():
                structure_out = Structure.Structure(0)
                model_out = Model.Model(0)
                fragment_file = StringIO()
                model_out.add(chain)
                structure_out.add(model_out)
                pio = PDBIO()
                pio.set_structure(structure_out)
                list_of_structures.append(structure_out) #keep Structure objects for coords update
        
            coords2 = self.get_dists(list_of_structures[0], True) #coords after superposition

            structs_coords_edited = self.update_coords(coord1, coords2, list_of_structures)

            for struct in structs_coords_edited:
                pio = PDBIO()
                fragment_file = StringIO()
                pio.set_structure(struct)
                pio.save(fragment_file)
                self.pdb_data_out.append(fragment_file.getvalue())


        # remove redundant files from temp directory
        temp_files = glob.glob(os.path.join(self.TEMP_DIR, '*'))

        for file_ in temp_files:
            os.remove(file_)

        # try to clean up the mess in R3D dir
        def list_files(*path):
            return glob.glob(os.path.join(SUPERIMPOSER_PATH, *path))

        temp_files = []
        for name in [name1, name2]:
            temp_files += list_files(name + "*")
            temp_files += list_files("PDBFiles", name + ".pdb")
            temp_files += list_files("Neighborhoods", name + "*")
            temp_files += list_files("PrecomputedData", name.upper() + "*")
            temp_files += list_files("R3D Align Output", "Final Mat Files", name + "*")
            temp_files += list_files("Sequence Alignments", name + "*")

        for file_ in temp_files:
            os.remove(file_)


if __name__ == "__main__":
    if len(sys.argv) < 7:
        print "USAGE: r3d_superimposer.py pdb_file1 model1_id chain1_id pdb_file2 model2_id chain2_id [output_file]"
        exit()
    if len(sys.argv) == 8:
        output_file = sys.argv.pop()
    else:
        output_file = "output.pdb"
    pdb_file1, model1_id, chain1_id, pdb_file2, model2_id, chain2_id = sys.argv[1:7]
    with open(pdb_file1, "rb") as f:
        pdb_dataA = f.read()
    with open(pdb_file2, "rb") as f:
        pdb_dataB = f.read()

    sup = R3D_Superimposer([pdb_dataA, pdb_dataB])
    sup.impose(int(model1_id), chain1_id, int(model2_id), chain2_id)
    with open(output_file, "wb") as f:
        f.write(sup.pdb_data_out[0])
        f.write(sup.pdb_data_out[1])

