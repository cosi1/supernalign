#!/usr/bin/env python

from StringIO import StringIO
from Bio.PDB import PDBParser, Model, Structure, PDBIO
import os, glob, sys
from global_config import SUPERALN_DIR, SMATCH_PATH
from superimposer import Superimposer


class Smatch_Superimposer(Superimposer):
    """
    Superimposes Fragments using Smatch
    """
    
    def impose(self, model_id1, chain_id1, model_id2, chain_id2):
        """
        Imposes Fragments 
        """

        name1, name2, pdb1, pdb2 = self.pdb_temp_files()

        SMATCH_OUTPUT = os.path.join(self.TEMP_DIR, "smatch_output.pdb")

        smatch_exec = os.path.join(SMATCH_PATH, "compare_structures.py")
        smatch_command = " ".join((smatch_exec, "--atoms=P,C1',C4'",
                                   pdb1, pdb2, "-o", SMATCH_OUTPUT))

        if os.name == 'nt':
            null = " > NUL 2>&1"
        elif os.name == 'posix':
            null = " > /dev/null 2>&1"
        else:
            null = ""
        smatch_command += null
        
        os.system(smatch_command)

        temp_files = [pdb1, pdb2]

        if not os.path.exists(SMATCH_OUTPUT):
            self.pdb_data_out = [self.pdb_data_A, self.pdb_data_B] 	    
            #print "Smatch: Fragment not aligned!" #XXX
        else:
            temp_files.append(SMATCH_OUTPUT)

            list_of_structures = []
        
            parser = PDBParser()
            structure_out_A = parser.get_structure("strA", pdb1)
            model_out_A = structure_out_A[0]
            chain_out_A = model_out_A[chain_id1]
            chain_out_A.id = "A"
            structure_out_B = parser.get_structure("strB", SMATCH_OUTPUT)
            model_out_B = structure_out_B[0]
            chain_out_B = model_out_B[chain_id2]
            chain_out_B.id = "B"
            list_of_structures = (structure_out_A, structure_out_B)
        
            for struct in list_of_structures:
                pio = PDBIO()
                fragment_file = StringIO()
                pio.set_structure(struct)
                pio.save(fragment_file)
                self.pdb_data_out.append(fragment_file.getvalue())

            # remove temporary files
            for f in temp_files:
                os.remove(os.path.join(self.TEMP_DIR, f))

