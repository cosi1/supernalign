#!/usr/bin/env python
from global_config import TEMP_DIR
from Clarnet import clarnet, pdb_test
from extres import Extres
from smatch import compare_structures as comp
import os, sys, glob, re


def temp_file(fname):
    return os.path.join(TEMP_DIR, fname)


class Prealign(object):
    """
    Prealigns input files using Clarnet and Smatch.
    Input:
        filenameA - reference structure file name
        filenameB - aligned structure file name
    """

    def __init__(self, filenameA, filenameB, output_filename = None):
        """
        Input:
            filenameA - reference structure
            filenameB - structure to be aligned
            output_filename - output file name
        """
        self.temp_files = []
        clustersA = self.get_clusters(filenameA, "A")
        clustersB = self.get_clusters(filenameB, "B")

        score_matrix = []
        for i, clusterA in enumerate(clustersA):
            row = []
            for j, clusterB in enumerate(clustersB):
                output_fname = temp_file("%i_%i.pdb" % (i, j))
                self.temp_files.append(output_fname)
                row.append(self.compare_fragments(clusterA[0][0],
                                                  clusterB[0][0],
                                                  output_fname))
            score_matrix.append(row)

        best_matches = self.find_best(score_matrix)
        if len(best_matches) == 0:
            exit("Structures cannot be prealigned!")
        
        new_clustersB = self.couple_neighbors(best_matches, clustersB)
        final_clustersB = self.superimpose_coupled(new_clustersB)
        
        basename = os.path.splitext(os.path.basename(filenameB))[0]
        all_files = [c[0] for c in zip(*final_clustersB)[0]]
        output_fname = output_filename or temp_file("%s_prealigned.pdb" %
                                                    basename)
        self.aligned_filename = self.join_fragments(all_files, output_fname)
        self.clean_up()

    def get_clusters(self, filename, prefix):
        """
        Returns list of clusters for a given structure.
        Input:
            filename - structure .pdb file name
            prefix - output files will begin with this prefix
        Output:
            list of clusters in the following format:
            [<file name(s)>, <residue numbers>, <cluster ID>, <matching ID>]
            e.g.:
            [
             [['/tmp/A_0.pdb'], [1, 2, 3, 4, 5, 6], 0, None],
             [['/tmp/A_1.pdb'], [7, 8, 9, 10, 11], 1, None],
             ...
            ]
        """
        cleaned_name, model = pdb_test.clean_pdb(filename)
        chain_id = model.chain_name
        graph_fname = temp_file("clarnet.json")
        graph_file = clarnet.run_clarna(cleaned_name, graph_file = graph_fname)
        contact_net = clarnet.load_contact_net_from_json(graph_file)
        filtered_net = clarnet.set_clarna_treshold(contact_net)
        chains_list = pdb_test.find_chain_gaps(model)
        clarnet.add_covalent_contacts(filtered_net, chains_list)
        clarnet.assign_myscores(filtered_net)
        basename = os.path.splitext(os.path.basename(cleaned_name))[0]
        clarnet.run_mcl(filtered_net, 1.3, basename, TEMP_DIR)
        clusters = clarnet.get_clusters(TEMP_DIR, basename)

        output = [] # function's output - list of clusters
        for j, cluster in enumerate(clusters):
            ids = [int(i[1:]) for i in cluster]
            ids.sort()
            e = Extres()
            e.extract_residues(cleaned_name, 0, chain_id, ids, True)
            cluster_fname = temp_file("%s_%i.pdb" % (prefix, j))
            with open(cluster_fname, "wb") as f:
                f.write(e.chain_fragment)
            self.temp_files.append(cluster_fname)
            output.append([[cluster_fname], ids, j, None])
        
        # Remove temporary files
        temp = temp_file(basename + ".*")
        temp_files = glob.glob(temp)
        temp = temp_file("out.%s.mci.*" % basename)
        temp_files += glob.glob(temp)
        temp_files.append(graph_fname)
        temp_files.append(cleaned_name)
        for fname in temp_files:
            try:
                os.remove(fname)
            except:
                pass
        return output

    def compare_fragments(self, filename1, filename2, output_fname):
        """
        Compares and superimposes fragments using Smatch.
        Input:
            filename1 - target structure
            filename2 - mobile structure
            output_fname - output file name
        Output:
            alignment score
        """
        inp1, name1, prog1 = comp.read_points(filename1, "P")
        inp2, name2, prog2 = comp.read_points(filename2, "P")
        try:
            res = comp.compare_structures_global(inp1, inp2, None,
                                                 rmsd_tolerance = 1.0)
        except IndexError:
            return 0.0
        score = comp.calculate_result_score(inp1, inp2, res)
        if res is not None:
            sc, matching, rms, tau = res
            comp.save_match_model(filename2, output_fname, [tau[0]], [tau[1]])
        return score

    def find_best(self, scores):
        """
        Finds best matches (above given threshold) in a score matrix.
        Input:
            scores - score matrix
        Output:
            list of best matches (row, column)
        """
        MIN_SCORE = 0.25
        len_i = len(scores)
        len_j = len(scores[0])
        best = []
        for n in range(min(len_i, len_j)):
            all_values = sum(scores, [])
            max_value = max(all_values)
            if max_value < MIN_SCORE:
                break
            which = all_values.index(max_value)
            i = which / len_j
            j = which % len_j
            best.append((i, j))
            # Fill the row and column with zeroes
            for k in range(len_i):
                scores[k][j] = 0
            for k in range(len_j):
                scores[i][k] = 0
        return best

    def couple_neighbors(self, best_matches, clusters):
        """
        Merges unmatched clusters with neighboring matched clusters.
        Input:
            best_matches - list of best matches
            clusters - list of all clusters
        Output:
            updated list of clusters
        """
        matched = zip(*best_matches)[1]
        matched_cl = [clusters[i] for i in matched]
        for i in range(len(matched_cl)):
            matched_cl[i][3] = best_matches[i][0]

        unmatched = [c for c in range(len(clusters))
                     if c not in matched]
        unmatched_cl = [clusters[i] for i in unmatched]

        for un in unmatched_cl:
            un_resi = un[1]
            cand_resi = zip(*matched_cl)[1]
            neighbor_num = self.find_neighbor(un_resi, cand_resi)
            neighbor = matched_cl.pop(neighbor_num)
            # Add this cluster to matched_cl
            neighbor[0].append(un[0][0])
            neighbor[1].extend(un[1])
            matched_cl.append(neighbor) # move coupled cluster to the end
        return matched_cl

    def find_neighbor(self, resi, candidate_resi):
        """
        Returns cluster closest to the given cluster
        (with the most contacts).
        Input:
            resi - list of residues in the cluster
            candidate_resi - list of residues in candidate clusters
        Output:
            number of the selected candidate cluster
        """
        left_anchors = [r - 1 for r in resi]
        right_anchors = [r + 1 for r in resi]
        anchors = left_anchors + right_anchors
        num_contacts = []
        for c_resi in candidate_resi:
            neigh_resi = [r for r in c_resi if r in anchors]
            num_contacts.append(len(neigh_resi))
        return num_contacts.index(max(num_contacts))

    def superimpose_coupled(self, clusters):
        """
        Superimposes coupled clusters and updates cluster list.
        Input:
            cluster list
        Output:
            updated cluster list
        """
        pattern = re.compile(r"B_([0-9+]+)\.pdb$")
        for i, cluster in enumerate(clusters):
            target_fname = temp_file("%i_%i.pdb" % (cluster[3], cluster[2]))
            if len(cluster[0]) == 1: # uncoupled
                # Replace file name with the already superimposed version
                clusters[i][0] = [target_fname]
                continue
            frag_fname = temp_file(self.join_fragments(cluster[0]))
            self.temp_files.append(frag_fname)
            cluster_numbers = re.findall(pattern, frag_fname)[0]
            output_fname = temp_file("%i_%s.pdb" %
                                     (cluster[3], cluster_numbers))
            self.compare_fragments(target_fname, frag_fname, output_fname)
            clusters[i][0] = [output_fname]
            self.temp_files.append(output_fname)
        return clusters

    def join_fragments(self, filenames, output_fname = None):
        """
        Joins fragments of .pdb data.
        Input:
            filenames - list of file names
            output_fname - output file name
        Output:
            output file name
        """
        def res_sorting(residue):
            try:
                return int(residue[22:26])
            except:
                return None

        data = []
        for fname in filenames:
            with open(fname, "rb") as f:
                data.extend([line for line in f.readlines() if line != "TER\n"
                             and line != "END\n"])
        data.sort(key = res_sorting)
        data.append("TER\nEND\n")
        
        if output_fname is None:
            pattern = re.compile(r"B_([0-9]+)\.pdb$")
            cluster_numbers = [re.findall(pattern, f)[0] for f in filenames]
            output_fname = temp_file("B_%s.pdb" % "+".join(cluster_numbers))
        with open(output_fname, "wb") as f:
            f.writelines(data)
        return output_fname        
    
    def get_aligned_structure(self):
        """
        Returns the aligned structure as a string:
        Output:
            aligned structure
        """
        with open(self.aligned_filename, "rb") as f:
            data = f.read()
        return data

    def clean_up(self):
        for fname in self.temp_files:
            try:
                os.remove(fname)
            except OSError:
                pass


if __name__ == "__main__":
    if len(sys.argv) < 4:
        exit("Usage: prealign.py input_file1 input_file2 output_file")
    fname1 = os.path.abspath(sys.argv[1])
    fname2 = os.path.abspath(sys.argv[2])
    p = Prealign(fname1, fname2, sys.argv[3])

