#!/usr/bin/env python
from Bio.PDB import *
from lib.moderna.moderna.moderna import *
import sys

def pdb_cheatsheet(m): #returns list of residues in order, even if there are atoms missing
	cheatsheet=[]
	for resi in m:
		no = resi.number

		insertion_code = resi.identifier.strip(str(no))
		if insertion_code=='':
			insertion_code=' '

		rn = resi.long_abbrev
		
		if rn=='X':
			try:
				pdb_cheat = chain[(' ', no, insertion_code)].resname.strip()
			except:
				for key in chain.child_dict.keys():
					if key[1] == no and key[2] == insertion_code:
						pdb_cheat = chain[key].resname.strip()
			cheatsheet.append(pdb_cheat)
		else:
			cheatsheet.append(rn)
	return cheatsheet


def clean_pdb(filename): #renumbers chain and removes modifications

	parser = PDBParser()
	structure = parser.get_structure('RNA', filename)

	chains = Selection.unfold_entities(structure, 'C')
	try: 
		chain = chains[0]
	except:
		sys.exit('PDB file is empty!')
	chain_id = chains[0].get_id()
	#print 'Number of chains in the structure provided: %i. Chain %s is going into pipeline.' % (len(chains), chain_id)

	m = load_model(chain, data_type='chain')
	#m.remove_all_modifications()
	#m.renumber_chain(1)
	
	output_name = '%s_unmod_renum.pdb' % filename.split('.')[0]
	m.write_pdb_file(output_name)

	return output_name, m


def find_chain_gaps(m): #returns list of residue numbers making complete chains

	'''
	gaps=[] 
	for ind, resi in enumerate(m.get_sequence()):
		if resi.short_abbrev == '_':
			gaps.append(ind)
			x = ind + 1
			print x
			#print ''.join(cheatsheet[:ind])
			#print ''.join(cheatsheet[ind:])
			#exchange_single_base(m[str(x)],'C')
	#print m.get_sequence()
 	'''
	seq = str(m.get_sequence())
	ind = 1
	numbers = []	
	for chunk in seq.split('_'):
		num=[]
		for letter in chunk:
			num.append(ind)
			ind+=1
		numbers.append(num)

	return numbers
	

if __name__=="__main__":
	structure = clean_pdb('/home/ejank/Desktop/supernalign/1H3E_unfrozen.pdb')[1]
	print find_chain_gaps(structure)

