### Author: Elzbieta Jankowska, IIMCB in Warsaw, ejank@genesilico.pl
### a wrapper (ClarnaGraph) for standalone Clarna is used. Seems to only run with NetworkX 1.7!


from lib.clarna.get_pairs.get_pairs import ClarnaGraph
import sys
import networkx as nx
from networkx.readwrite import json_graph
import json
from networkx.drawing import nx_pylab
import numpy
from scipy.cluster import hierarchy
from scipy.spatial import distance
from collections import defaultdict
import subprocess
import os
import re
import string

from pdb_test import *


'''
"prg": "prg", "weight": "weight", "reference": "reference", "full_desc": "full_desc", "n_type": "n_type", "is_alt": "is_alt", "source": "source", "desc": "desc", "type": "type", "target": "target"
'''
def run_clarna(filepath, graph_file='clarnagraph.json'): #runs Clarna locally
	clarna_graph = ClarnaGraph(filepath, graph_file)
	return clarna_graph.graph_fname

def load_contact_net_from_json(filepath): #Load contact map from ClaRNA results in JSON format - can be from webserver but it's better to use standalone version as run_clarna cleans PDB file first and there is no problem with insertion codes.
	jsongraph = open(filepath)
	graph = json.loads(''.join(jsongraph.readlines()).replace('?', '').replace('"directed": true', '"directed": false')) #remove all '?', that is disregard ClaRNA treshold, make the graph undirected
	contact_net = json_graph.node_link_graph(graph)
	return contact_net

def set_clarna_treshold(graph, treshold=0.6): #Set a treshold on loaded contacts, default 0.6
	for n1, n2, key, attr in graph.edges(data=True, keys=True):
		if attr['weight']<treshold:
			graph.remove_edge(n1, n2, key=key)
	return graph


def add_covalent_contacts(graph, chains_list): #Add edges representing covalent bonds between nucleotides
	
	letter = str(graph.nodes()[0])[0]
	assert letter in string.ascii_letters
	
	for item in chains_list:
		if len(item)==1:
			continue
		for v, w in zip(item[:-1], item[1:]):
			#print [v, w]
			node_a = letter+str(v)
			node_b = letter+str(w)
			graph.add_edge(node_a, node_b, myscore = 0.5, cc = 'covalent')


def assign_myscores(graph): #Assign scores to all contacts
	contact_classes = {
	'WW_cis': 'canonical_bp', 
	'WW_tran': 'non-canonical_bp', 
	'HW_cis': 'non-canonical_bp', 
	'WH_cis': 'non-canonical_bp', 
	'HW_tran': 'non-canonical_bp', 
	'WH_tran': 'non-canonical_bp', 
	'SW_cis': 'non-canonical_bp', 
	'WS_cis': 'non-canonical_bp', 
	'SW_tran': 'non-canonical_bp',
	'WS_tran': 'non-canonical_bp', 
	'HH_cis': 'non-canonical_bp', 
	'HH_tran': 'non-canonical_bp', 
	'HS_cis': 'non-canonical_bp', 
	'SH_cis': 'non-canonical_bp', 
	'HS_tran': 'non-canonical_bp', 
	'SH_tran': 'non-canonical_bp', 
	'SS_cis': 'non-canonical_bp', 
	'SS_tran': 'non-canonical_bp', 
	'>>': 'stacking', 
	'<>': 'stacking',
	'<<': 'stacking',
	'><': 'stacking', 
	'H_0BPh': 'base-phosphate_int', 
	'S_1BPh': 'base-phosphate_int', 
	'W_345BPh': 'base-phosphate_int', 
	'W_6BPh': 'base-phosphate_int', 
	'H_789BPh': 'base-phosphate_int', 
	'SW_2BPh': 'base-phosphate_int', 
	'H_0BR': 'base-ribose_int', 
	'S_1BR': 'base-ribose_int', 
	'W_345BR': 'base-ribose_int', 
	'W_6BR': 'base-ribose_int', 
	'H_789BR': 'base-ribose_int', 
	'SW_2BR': 'base-ribose_int', 
	'diagonal-c': 'other', 
	'diagonal-nc-ww': 'other', 
	"phosphate-stacking1": 'other',
        "phosphate-stacking2": 'other',
	'base-ribose-stacking': 'other', 
	'long-stacking-c': 'other'}#

	contact_scores = {'covalent': 0.5, 'canonical_bp': 2, 'non-canonical_bp': 1, 'stacking': 1, 'base-phosphate_int': 1, 'base-ribose_int': 1, 'other': 1}

	for edge in graph.edges(data=True):

		try:
			edge[2]['desc']
		except:
			continue

		assert edge[2]['desc'] in contact_classes.keys()
		edge[2]['cc'] = contact_classes[edge[2]['desc']]
		edge[2]['myscore'] = contact_scores[edge[2]['cc']]


def merge_multiedges(graph): #merge multiedges of the graph - for methods that require singleedge graphs. In case of multiedges merged edge has the myscore of the strongest multiedge (max myscore observed)
	graph_copy = nx.Graph()

	for u,v,data in graph.edges_iter(data=True):
		w = data['myscore']
		if graph_copy.has_edge(u,v):
			if w > graph_copy[u][v]['myscore']:
				graph_copy[u][v]['myscore'] = w
		else:
			graph_copy.add_edge(u, v, myscore=w)
		if w==2:
			graph_copy[u][v]['color'] = 'red'
		else:
			graph_copy[u][v]['color'] = 'blue'

	for node in graph.nodes(data=True):
		try:
			label = node[1]['resname']
			graph_copy.node[node[0]]['resname']=label
		except:
			continue
	return graph_copy

def draw_this_graph(graph, modularity = False, clustering = False): #Draw the graph! Size of nodes corresponds to clustering coefficient, edge width to edge myscore
	#parameters: 
	#modularity - show communities based on modularity optimisation - louvian algorithm, requires community package, http://perso.crans.org/aynaud/communities/community.py
	#clustering - show clustering coefficient of each node represented as its size
	if modularity==True or clustering==True: #both modularity optimisation and clustering coefficient calculation require a graph without multiedges, so a copy with myscores added up is created here
		filtered_copy = merge_multiedges(graph)

	if clustering == True:
		clust = nx.clustering(filtered_copy, weight='myscore')
		nodesize = []
		for node in graph.nodes():
			nodesize.append(clust[node]*1000)
	else: nodesize=[100 for i in range(0, 100)]

	if modularity == True:
		import community
		partition = community.best_partition(filtered_copy)
		for i in set(partition.values()):
			members = list_nodes = [nodes for nodes in partition.keys() if partition[nodes] == i]
		values = [partition.get(node) for node in filtered_copy.nodes()]
	else: values=[]

	node_labels={}
	for node in filtered_copy.nodes(data=True):
		node_labels[node[0]] = '_'.join([node[0], node[1]['resname']])
	
	edgewidth = []
	for ind, edge, data in filtered_copy.edges(data=True):
		edgewidth.append(data['myscore'])

	edgecolor = []
	for ind, edge, data in filtered_copy.edges(data=True):
		#print data
		edgecolor.append(data['color'])

	pos=nx.graphviz_layout(filtered_copy) #pretty layout

	fig = plt.figure(figsize=(15,9))
	plt.xticks([]); plt.yticks([])
	plt.title('Modularity optimisation clustering\n %s\n Canonical base pairs in red' % name)

	nx.draw_networkx_edges(filtered_copy, pos, width = edgewidth, edge_color = edgecolor) #edges width corresponds to myscore
	nx.draw_networkx_nodes(filtered_copy, pos, cmap = plt.get_cmap('jet'), node_color = values, node_size = nodesize)# if you want to show modularity optimisation
	nx.draw_networkx_labels(filtered_copy, pos, font_size=10, labels = node_labels) #pretty pretty labels

	plt.show()


def get_cluster_classes(den, label='ivl'): #SOURCE: http://nxn.se/post/90198924975/extract-cluster-elements-by-color-in-python Valentine Svensson
	cluster_idxs = defaultdict(list)
	for c, pi in zip(den['color_list'], den['icoord']):
		for leg in pi[1:3]:
			i = (leg - 5.0) / 10.0
			if abs(i - int(i)) < 1e-5:
				cluster_idxs[c].append(int(i))

	i_l=defaultdict(list)
	for c, l in cluster_idxs.items():
		labels = [den[label][i] for i in l]
		for item in labels:			
			i_l[c].append(item)
	
	return i_l


def show_hierarchical_clustering(graph, plot=False, save_to_file=True): #draw a dendrogram showing a hierarchical clustering of the nodes, based on shortest path length matrix. 
	#Better results than modularity optimisation, still doesn't take myscore under consideration, though
	#plot=True shows the dendrogram
	path_length=nx.all_pairs_shortest_path_length(graph)
	n = len(graph.nodes())
	x = [int(item[1:]) for item in graph.nodes()]

	for item in graph.nodes():
		assert int(item[1:])>=0

	findlist = sorted(graph.nodes(), key = lambda item: int(item[1:]))
	find = {}
	z=0
	for node in findlist:
		find[node] = z
		z+=1
	findlabel = {v: k for k, v in find.items()}
	distances=numpy.zeros((z,z))
	for u,p in path_length.iteritems():
		for v,d in p.iteritems(): # v - node, d - distance between u and v
			distances[find[u]][find[v]] = d

	sd = distance.squareform(distances)
	hier = hierarchy.average(sd)
	info = hierarchy.dendrogram(hier, labels=findlist, leaf_font_size=10, leaf_rotation=30, no_plot=True)

	hierarchy_colors_dic = get_cluster_classes(info)
	hierarchy_colors = hierarchy_colors_dic.items()
	if save_to_file:
		out = open('clustering.txt', 'w')
		for k, v in hierarchy_colors:
			#print k, v
			values = ', '.join(v)
			out.write('%s: %s\n' % (k, values))
	if plot==True:
		import matplotlib
		matplotlib.pylab.show()
	return hierarchy_colors


def hierarchy_graph(graph, hierarchy_colors):
	filtered_copy = merge_multiedges(graph)
	graph = filtered_copy

	node_colors=[]
	dic={}
	for item in hierarchy_colors:
		for it in item[1]:
			dic[it] = item[0]
		
	for node in graph.nodes(data=True):
		node_colors.append(dic[str(node[0])])

	edgecolor = []
	for ind, edge, data in graph.edges(data=True):
		#print data
		edgecolor.append(data['color'])

	node_labels={}
	for node in graph.nodes(data=True):
		node_labels[node[0]] = '_'.join([node[0], node[1]['resname']])
	
	edgewidth = []
	for ind, edge, data in graph.edges(data=True):
		edgewidth.append(data['myscore'])

	pos=nx.graphviz_layout(graph) #pretty layout

	import pylab as plt
	fig = plt.figure(figsize=(15,9))
	plt.xticks([]); plt.yticks([])
	plt.title('Hierarchy clustering\n %s\n Canonical base pairs in red' % name)

	nx.draw_networkx_edges(graph, pos, width = edgewidth, edge_color=edgecolor) #edges width corresponds to myscore
	nx.draw_networkx_nodes(graph, pos, cmap = plt.get_cmap('jet'), node_color = node_colors)#, node_size = nodesize)# if you want to show modularity optimisation
	nx.draw_networkx_labels(graph, pos, font_size=10, labels = node_labels) #pretty pretty labels

	#plt.show()	
	plt.savefig('%s/%s_hierarchy.png' % (path_to_save, name))

def run_mcl(graph, I, filepath, path_to_save=None):

	filename = os.path.basename(filepath).split('.')[0]
	if path_to_save is None:
		path_to_save = os.path.split(filepath)[0]
	outpath = os.path.join(path_to_save, filename)

	nx.write_edgelist(merge_multiedges(graph), '%s.abc' % outpath, data=['myscore'])
	command_mcxload = "mcxload -abc %s.abc --stream-mirror -o %s.mci -write-tab %s.tab" % (outpath, outpath, outpath)
	command_mcl = "mcl %s.mci -I %f --d" % (outpath, I)
	command_mcxdump = "mcxdump -icl %s/out.%s.mci.I%i -tabr %s.tab -o %s.res" % (path_to_save, filename, int(I*10), outpath, outpath)

	DEVNULL = open(os.devnull, 'wb')

	proc = subprocess.Popen(command_mcxload, shell=True, stdin=subprocess.PIPE, stdout=DEVNULL, stderr=DEVNULL)#stdout=subprocess.PIPE)
	proc.communicate()

	proc = subprocess.Popen(command_mcl, shell=True, stdin=subprocess.PIPE, stdout=DEVNULL, stderr=DEVNULL)# stdout=subprocess.PIPE)
	proc.communicate()

	proc = subprocess.Popen(command_mcxdump, shell=True, stdin=subprocess.PIPE, stdout=DEVNULL, stderr=DEVNULL)# stdout=subprocess.PIPE)
	proc.communicate()


def draw_mcl(graph, I, input_path, path_to_save=None):

	filtered_copy = merge_multiedges(graph)
	graph = filtered_copy

	pos=nx.graphviz_layout(graph) #pretty layout
	
	filename = os.path.basename(input_path).split('.')[0]
	if path_to_save is None:
		path_to_save = os.path.split(input_path)[0]

	edgecolor = []
	for ind, edge, data in graph.edges(data=True):
		#print data
		edgecolor.append(data['color'])

	node_colors=[]
	dic={}
	with open('%s/%s.res' % (path_to_save, filename), 'r') as spam:
		for ind, line in enumerate(spam):
			for node in line.split('\t'):
				dic[node.strip()]=ind
	
	for node in graph.nodes(data=True):		
		node_colors.append(dic[node[0]])

	node_labels={}
	for node in graph.nodes(data=True):
		#print node
		node_labels[node[0]] = '_'.join([node[0], node[1]['resname']])

	import pylab as plt
	fig = plt.figure(figsize=(15,9))
	plt.xticks([]); plt.yticks([])
	plt.title('MCL clustering with I=%s \n %s\n Canonical base pairs in red' % (str(I), filename))

	nx.draw_networkx_edges(graph, pos, edge_color = edgecolor)
	nx.draw_networkx_nodes(graph, pos, cmap = plt.get_cmap('jet'), node_color = node_colors)
	nx.draw_networkx_labels(graph, pos, font_size=10, labels = node_labels) #pretty pretty labels

	#plt.show()
	plt.savefig('%s/%s_mcl_%s.png' % (path_to_save, filename, str(int(I*10))))


def pymol_commands(input_path):
	path_to_save = os.path.split(input_path)[0]
	name = os.path.basename(input_path).split('.')[0]
	with open('%s/%s.res' % (path_to_save, name), 'r') as spam:
		for line in spam:
			res = [item.strip('\t') for item in re.split(r'\D', line) if item !='']
			print 'select resi %s' % '+'.join(res)


def get_clusters(path_to_save, filename):
	clusters = []
	with open('%s/%s.res' % (path_to_save, filename), 'r') as spam:
		for line in spam:
			cluster = []
			for node in line.split('\t'):
				cluster.append(node.strip())
			clusters.append(cluster)
	return clusters

### DO THE JOB

if __name__ == "__main__":

	input_path = sys.argv[1]
	name = os.path.basename(input_path).split('.')[0]
	extension = os.path.basename(input_path).split('.')[1]
	folder = os.path.dirname(input_path)
	
	m = clean_pdb(input_path)
	unmodified_renumbered = m[0]#removes modifications from input structure and renumbers it
	chains_list = find_chain_gaps(m[1])
	
	input_path = unmodified_renumbered


	if extension=='pdb':
		graph = run_clarna(input_path, graph_file='%s/clarnagraph.json' % folder)
	elif extension=='json':
		graph = input_path
	else:
		print "Input file should be either a 3D structure in .pdb format or a contact graph in .json format"
	
	contact_net = load_contact_net_from_json(graph)
	filtered_net = set_clarna_treshold(contact_net)
	add_covalent_contacts(filtered_net, chains_list)
	assign_myscores(filtered_net)
	#draw_this_graph(filtered_net, modularity=True, clustering=True)
	#show_hierarchical_clustering(filtered_net)
	#nx.write_edgelist(merge_multiedges(filtered_net), 'unfrozen.abc',data=['myscore'])
	#hierarchy_graph(filtered_net, show_hierarchical_clustering(filtered_net))
	
	I=1.3

	run_mcl(filtered_net, I, input_path)
	#draw_mcl(filtered_net, I, input_path)
	pymol_commands(input_path)

	
