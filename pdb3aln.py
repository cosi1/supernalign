#!/usr/bin/env python

from Bio.PDB import *
from math import sqrt
import sys
from StringIO import StringIO


try:
    from moderna.RNAResidue import RNAResidue
    MODERNA_INSTALLED = True
except ImportError:
    import warnings
    warnings.simplefilter("ignore") # disable BioPython warning messages
    MODERNA_INSTALLED = False


class Pdb3align(object):

    TWO_ATOM_DISTANCE = True   # should we calculate distance using two atoms?
    THRESHOLD = 12.0 # maximum distance (in Angstroms) of matching residues
    
    if TWO_ATOM_DISTANCE:
        THRESHOLD = sqrt(2) * THRESHOLD  # max distance for 6D match
        ATOM1 = "C1'"                    # start of "6D vector"
        ATOM2 = "N9/N1"                     # end of "6D vector"
    else:
        ATOM = "C1'"
    
    def __init__(self):
        self.basesA = ""
        self.basesB = ""
        self.sum_map = []
        self.dist_map = []
        self.new_path = []
        self.whole_path = []
        self.wh_path_indices = []
        self.sequences = []
        self.distances = [] 
    
    def get_base_name(self,res):
        if MODERNA_INSTALLED:
            r = RNAResidue(res)
            return r.original_base.lower()
        return res.get_resname().strip().lower()

    def get_atom(self, res, atom_string):
        '''
        Returns atom object extracted from res, depending on base type.
        atom_string = "<purine_atom_name>[|<pyrimidine_atom_name>]"
        '''
        atoms = atom_string.split("/")
        if len(atoms)==1:
            return res[atom_string]

        if MODERNA_INSTALLED:
            r = RNAResidue(res)
            is_purine = True if r.purine else False
        else:
            res_name = res.get_resname().strip().lower()
            is_purine = True if res_name in ("a", "g") else False

        if is_purine:
            return res[atoms[0]]
        else:
            return res[atoms[1]]

    def distance(self,vector1, vector2):
        if len(vector1)!=len(vector2):
            raise Exception("Vector lengths do not match!")
        sigma = 0
        for i in range(len(vector1)):
            sigma += (vector2[i] - vector1[i]) ** 2
        return sqrt(sigma)
        
    def align_input(self, filename1, model1_id, chain1_id, filename2, model2_id, chain2_id, dec):
        '''
        Prepares pdb3aln input
        '''
        if dec == False:
            chain1 = StringIO(filename1)
            chain2 = StringIO(filename2)
        else:
            chain1 = filename1
            chain2 = filename2
        parser = PDBParser()
        struct1 = parser.get_structure("str1",chain1)
        model1 = struct1[int(model1_id)]
        struct2 = parser.get_structure("str2",chain2)
        model2 = struct2[int(model1_id)]
        chainA = model1[chain1_id]
        chainB = model2[chain2_id]
        chains = (chainA, chainB)
        return chains
    
    def get_coordinates(self, chains):
        '''
        Gets coordinates of ATOM1 and ATOM2 for all residues in both chains
        '''
        coords = [[], []]
        for i in (0, 1):
            for res in chains[i]:
                base_name = self.get_base_name(res)
                a1 = False
                try:
                    if self.TWO_ATOM_DISTANCE:
                        a1 = self.get_atom(res, self.ATOM1)
                        a2 = self.get_atom(res, self.ATOM2)
                    else:
                        a1 = a2 = self.get_atom(res, self.ATOM)
                except KeyError:
                    try:
                        a1 = a2 = res["P"]
                    except KeyError:
                        pass
                if a1!=False:
                    coords[i].append((res.id[1], base_name,
                        a1.get_coord(), a2.get_coord()))
        return coords            
    
    def match_coordinates(self, coords):
        '''
        Iterate over residues in chain 1 and match coordinates to chain2 
        '''
        for pos0, base_coord in enumerate(coords[0]):         # iterate over base residues
            self.dist_map.append([])
            for pos1, match_coord in enumerate(coords[1]):    # iterate over match residues
                base_vector = list(base_coord[2])             # base self.ATOM1 coords
                match_vector = list(match_coord[2])           # match self.ATOM1 coords
                if self.TWO_ATOM_DISTANCE:
                    base_vector.extend(list(base_coord[3]))   # base self.ATOM2 coords
                    match_vector.extend(list(match_coord[3])) # match self.ATOM2 coords
                dist = self.distance(base_vector, match_vector)
                self.dist_map[pos0].append(dist)
                
    def find_optimal_path(self):
        '''
        Populate cumulative matrix (sum_map) and iterate through the cumulative matrix to find the optimal path
        '''
        max_score = -1
        start_i = None
        start_j = None 
        for j, row in enumerate(self.dist_map):
            sum_row = []
            for i, col in enumerate(row):
                neighbors = [0]
                if i>0:
                    neighbors.append(sum_row[i-1])
                if j>0:
                    neighbors.append(self.sum_map[j-1][i])
                # in case the distance was zero...
                row_i = row[i]==0 and 1e-10 or row[i]       
                # the lower the distance, the higher the score
                score = max(neighbors) + 1 / row_i
                sum_row.append(score)
                if score>max_score:
                    max_score = score
                    start_i = i
                    start_j = j
            self.sum_map.append(sum_row)

        i = start_i
        j = start_j
        if i is None or j is None:
            return []
        path = [(i, j)]

        # iterate through 
        while i>0 or j>0:
            neighbors = (self.sum_map[j][i-1], self.sum_map[j-1][i])
            max_neighbor = max(neighbors)
            if j==0:
                i -= 1
            elif i==0:
                j -= 1
            else:
                if max_neighbor==neighbors[0]:  # go left
                    i -= 1
                elif max_neighbor==neighbors[1]:    # go up
                    j -= 1
            path.append((i, j))
        return path
        
    def remove_too_distant(self,path):
        '''
        Remove too distant pairs
        '''
        cut_path = []
        for i, j in path:
            if self.dist_map[j][i]<=self.THRESHOLD:
               cut_path.append((i, j))

        return cut_path
    
    def remove_redundant(self, cut_path):
        '''
        Remove redundant positions (quick and dirty method)
        '''
        if len(cut_path)==0:
            return
        B, A = zip(*cut_path)
        A = list(A)
        B = list(B)
        dists = [self.dist_map[j][i] for i, j in cut_path]
        A.append(-1)
        B.append(-1)
        dists.append(None)
        first_a = min_a = A[0]
        first_b = min_b = B[0]
        min_dist = dists[0]

        for i in range(1, len(A)):
            if A[i]!=first_a and B[i]!=first_b:
                # min_dist=None means that no pair has "jumped in" as first
                # so far (they still share one of the IDs with the last pair)
                if min_dist is not None:
                    self.new_path.append((min_b, min_a))
                    min_dist = None
                # We don't want to get doubles in new_path, do we?
                if A[i]==min_a or B[i]==min_b:
                    continue
                first_a = A[i]
                first_b = B[i]
            if min_dist is None or dists[i]<min_dist:
                min_a = A[i]
                min_b = B[i]
                min_dist = dists[i]
   
    def get_whole_path_indices(self, coords):
        '''
        Indices of whole_path
        '''
        for row in self.whole_path:
            if(row[0] != None and row[1] == None):
                self.wh_path_indices.append((coords[1][row[0]][0], None))
                self.sequences.append((coords[1][row[0]][1], None))
            elif(row[0]==None and row[1]!=None):
                self.wh_path_indices.append((None, coords[0][row[1]][0]))
                self.sequences.append((None, coords[0][row[1]][1]))
            elif(row[0]==None and row[1]==None):
                pass
            else:
                self.wh_path_indices.append((coords[1][row[0]][0], coords[0][row[1]][0]))
                self.sequences.append((coords[1][row[0]][1], coords[0][row[1]][1]))

    def generate_alignment(self, coords):
        '''
        Generate sequence alignment
        '''
        if len(self.new_path)>0:
            B, A = zip(*self.new_path)
        else:
            B, A = [[], []]

        A = list(A[::-1])
        B = list(B[::-1])

        for i in range(len(coords[0])):
            if i not in A:
                A.insert(i, i)
                B.insert(i, None)

        for i in range(len(coords[1])):
            if i not in B:
                try:
                    ind = B.index(i - 1)
                except ValueError:
                    ind = 0
                A.insert(ind + 1, None)
                B.insert(ind + 1, i)
        
        old_indices = [B.index(i) for i in range(max(B)+1)]        
        new_indices = sorted(old_indices)
        A2 = A[:]
        B2 = B[:]
        for i in range(len(new_indices)):
            A2[new_indices[i]] = A[old_indices[i]]
            B2[new_indices[i]] = B[old_indices[i]]
        A = A2
        B = B2
        
        for i in range(len(A)):
            self.whole_path.append((B[i], A[i]))
            
        return A, B
        
    def display_alignment(self, A, B, coords):
        '''
        Sort alignment - sometimes the beginning of B can be messed up
        '''
        self.basesA = ""
        self.basesB = ""

        for i in range(len(A)):
            self.basesA += A[i] is not None and coords[0][A[i]][1] or "-"
            self.basesB += B[i] is not None and coords[1][B[i]][1] or "-"
        
    def get_dist_path(self):
        for i,j in self.whole_path:
            if(i == None and j == None):
                pass
            if((i != None and j == None)or(i == None and j != None)):
                self.distances.append(None)
            if(i != None and j != None):
                self.distances.append(self.dist_map[j][i])
        
    def align(self, filename1, model1_id, chain1_id, filename2, model2_id, chain2_id,dec): 
        chains = self.align_input(filename1, model1_id, chain1_id, filename2, model2_id, chain2_id, dec)
        coords = self.get_coordinates(chains)
        self.match_coordinates(coords)
        path = self.find_optimal_path()
        cut_path = self.remove_too_distant(path)
        self.remove_redundant(cut_path)
        A1, B1 = self.generate_alignment(coords)
        self.display_alignment(A1, B1, coords)
        self.get_whole_path_indices(coords)
        self.get_dist_path()

        return coords


def write_csv(wh_ids, distances):
    f = open("distances.csv", "wb")
    for row in range(len(distances)):
        f.write(str(wh_ids[row][0])+'\t'+str(wh_ids[row][1])+'\t'+str(distances[row])+'\n')
    f.close()


if __name__=="__main__":
    
    if len(sys.argv)<6:
        exit("USAGE: pdb3aln.py <pdb_file1> <model1_id> <chain1_id> [<pdb_file2>] <model2_id> <chain2_id>")
    elif len(sys.argv)==6:
        filename1, model1_id, chain1_id, model2_id, chain2_id = sys.argv[1:]
        filename2 = filename1
    else:
        filename1, model1_id, chain1_id, filename2, model2_id, chain2_id = sys.argv[1:7]


    aln = Pdb3align()
    coords = aln.align(filename1, model1_id, chain1_id, filename2, model2_id, chain2_id, True)
    write_csv(aln.wh_path_indices, aln.distances)

    print ">chainA"
    print aln.basesA
    print ">chainB"
    print aln.basesB


