#!/usr/bin/env python

from StringIO import StringIO
from Bio.PDB import PDBParser, Model, Structure, PDBIO
from global_config import SUPERALN_DIR, SUPERIMPOSER_PATH
from superimposer import Superimposer, SuperimposerException
import os, glob, sys, subprocess


class SARA_Superimposer(Superimposer):
    """
    Superimposes Fragments using SARA
    """
    
    def impose(self, model_id1, chain_id1, model_id2, chain_id2):
        """
        Imposes Fragments 
        """

        coords1 = self.get_dists(self.pdb_data_A, False) #coords before superposition
        name1, name2, pdb1, pdb2 = self.pdb_temp_files()

        SARA_OUTPUT = os.path.join(self.TEMP_DIR, "sara_output") # without extension
        SARA_OUTPUT_PDB = SARA_OUTPUT + ".pdb"

        sara_sh = os.path.join(SUPERALN_DIR, "sara.sh")

        command = " ".join((sara_sh, SUPERIMPOSER_PATH,
                            pdb1, chain_id1,
                            pdb2, chain_id2,
                            SARA_OUTPUT))

        nullDev = open(os.devnull, "w")
        errCode = subprocess.call(command, stdout = nullDev, stderr = nullDev, shell = True)

        if errCode != 0:
            raise SuperimposerException("""
SARA couldn't be run. Please check SUPERIMPOSER_PATH in the config file.""")

        if not os.path.exists(SARA_OUTPUT_PDB):
            self.pdb_data_out = [self.pdb_data_A, self.pdb_data_B] 	    
            #print "SARA: Fragment not aligned!" #XXX
        else:
            list_of_structures = []
        
            parser = PDBParser()
            sara_structure = parser.get_structure("sara", SARA_OUTPUT_PDB)
            sara_model = sara_structure[0]
            for chain in sara_model.child_list:
                structure_out = Structure.Structure(0)
                model_out = Model.Model(0)
                model_out.add(chain)
                structure_out.add(model_out)
                pio = PDBIO()
                pio.set_structure(structure_out)
                list_of_structures.append(structure_out) #keep Structure objects for coords update

            coords2 = self.get_dists(list_of_structures[0], True) #coords after superposition
            structs_coords_edited = self.update_coords(coords1, coords2, list_of_structures)

            for struct in structs_coords_edited:
                pio = PDBIO()
                fragment_file = StringIO()
                pio.set_structure(struct)
                pio.save(fragment_file)
                self.pdb_data_out.append(fragment_file.getvalue())

        # remove temporary files
        temp_files = [pdb1, pdb2, SARA_OUTPUT_PDB]
        for tf in temp_files:
            if os.path.exists(tf):
                os.remove(tf)
            
