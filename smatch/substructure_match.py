#!/usr/bin/env python
"""
Find 3d pattern P in target object T
only rigid transformations are available
(pattern can be translated and/or rotated)

Required libraries:
 - BioPython (http://biopython.org/)
 - NumPy (http://numpy.scipy.org/)
 in near future:
 - NetworkX (http://networkx.lanl.gov/)
"""
#pylint: disable=E0611,E1101,E1102,E1121
import os
import re
from operator import itemgetter
from optparse import OptionParser
from scipy.spatial import KDTree
import simplejson as json
import gzip
#
import networkx as nx



# from biopython
from Bio import PDB
from Bio.SVDSuperimposer import SVDSuperimposer
from WSVDSuperimposer import WSVDSuperimposer

from numpy import dot, sqrt, array
from sm_utils import find_files_in_dir

VERSION = "0.3"
SIMRNA_ATOMS = "P|C4',N1,N9"

def read_points(fn, accepted_atoms='P'):
    """read set of points from file fn,
    file could be plain text file (each point in seperate line)
    or PDB file"""
    name = os.path.basename(fn)
    if '|' in accepted_atoms:
        a = accepted_atoms.split("|")
        return ([read_points(fn,aa)[0] for aa in a], name, True)
    atoms = accepted_atoms.split(",")
    p = []
    if fn.split('.')[-1] == 'gz':
        f = gzip.open(fn, "r")
    else:
        f = open(fn, "r")

    for line in f.readlines():
        if re.match("^#\s*(.*)\s*$", line):
            name = line[1:].strip()
            continue

        if re.match("^(ATOM|HETATM).*", line): # PDB syntax
            atom = line[13:16].strip()
            if accepted_atoms != '*' and not (atom in atoms):
                continue
            line = line[26:38]+" "+line[38:46]+" "+line[46:55]
        elif re.match("^(TER|CRYST|SCALE|ANISOU|CONNECT).*", line):
            continue

        line = line.strip()
        point = []
        for token in re.split("\s+", line):
            if re.match("^\-?\d+(\.\d+)?$", token):
                try:
                    point.append(float(token))
                except Exception, e:
                    print "INVALID token: %s" % token
                    raise e
        if len(point)>0:
            p.append(point)
    f.close()
    return (p, name, False)

def write_points(fn, p, name=None):
    """save set of points to file fn (currently it is not used)"""
    if name is None:
        name = os.path.basename(fn)
    f = open(fn, "w")
    f.write("# %s\n" % name)
    for point in p:
        f.write(" ".join(["%.8f"%q for q in point]))
        f.write("\n")
    f.close()

def dist(v1, v2):
    """calculate Euclidean distance between points v1 and v2"""
    diff = v1 - v2
    return sqrt(sum(diff*diff))

def find_pivot_pattern(P):
    """find good pivot (subpattern of length 3) for pattern P,
    currently we use 3 most distant points"""
    assert len(P)>=3
    m = len(P)
    max_dist = -1
    res = None
    # select the most distant pair of points
    for i in xrange(m-1):
        for j in xrange(i+1, m):
            curr_dist = dist(P[i], P[j])
            if (curr_dist > max_dist):
                res = [P[i], P[j]]
                max_dist = curr_dist
    assert len(res)==2
    # select third point
    max_dist = -1
    max_k = None
    for k in xrange(m):
        dist1 = dist(res[0], P[k])
        dist2 = dist(res[1], P[k])
        if dist1 > 0 and dist2 > 0 and dist1 + dist2 > max_dist:
            max_k = k
            max_dist = dist1 + dist2
    assert max_k is not None
    res.append(P[max_k])
    assert len(res)==3
    return array(res,'f')

def find_pivot_matches(T, pivot, rmsd_tolerance):
    """find set of triples [i,j,k] such that T[i],T[j],T[k]
       matches pivot (of length 3)"""
    assert len(pivot)==3
    n = len(T)
    d1 = dist(pivot[0], pivot[1])
    d2 = dist(pivot[0], pivot[2])
    res = []
    neigh1 = [ [] for i in xrange(n) ]
    neigh2 = [ [] for i in xrange(n) ]
    for i in xrange(n-1):
        for j in xrange(i+1, n):
            d = dist(T[i], T[j])
            if abs(d-d1) <= (1.5*rmsd_tolerance):
                neigh1[i].append(j)
                neigh1[j].append(i)
            if abs(d-d2) <= (1.5*rmsd_tolerance):
                neigh2[i].append(j)
                neigh2[j].append(i)
    d3 = dist(pivot[1], pivot[2])
    for i in xrange(n):
        for n1 in neigh1[i]:
            for n2 in neigh2[i]:
                if n1 != n2:
                    d = dist(T[n1], T[n2])
                    if abs(d-d3) <= (1.5*rmsd_tolerance):
                        res.append([i, n1, n2])
    return res

def compute_pivot_transformation(target, pivot, pivot_match):
    """compute rotation and translation that transforms pivot to target[pivot_match]"""
    assert len(pivot) == len(pivot_match)
    t = array([target[i] for i in pivot_match], 'f')
    sup = SVDSuperimposer()
    sup.set(t, pivot)
    sup.run()
    return sup.get_rotran()

def finetune_transformation(target, pattern, rot, tran, pattern_weights=None, t_colors=None, p_colors=None):
    """slightly update rot and tran to pattern, to match as closely as possible
       target"""
    n = len(target)
    m = len(pattern)

    # rotate whole pattern
    p = dot(array(pattern,'f'), rot) + tran

    # find the best matching, WARNING! if match is imperfect this will not work
    # TODO -- do something if the same vertex is added twice to curr_match
    # TODO -- do something if the distance between P[i] and curr_match[T[i]] is too big
    match = []
    if t_colors is None and p_colors is None:
        tree = KDTree(target)
        for j in xrange(m):
            (d,k) = tree.query(p[j])
            match.append(k)
    else:
        targets = {}
        org_num = {}
        for i in xrange(n):
            if not targets.has_key(t_colors[i]):
                targets[t_colors[i]]=[]
                org_num[t_colors[i]]=[]
            targets[t_colors[i]].append(target[i])
            org_num[t_colors[i]].append(i)
        trees = {}
        for c in t_colors:
            trees[c] = KDTree(targets[c])
        for j in xrange(m):
            (d, k) = trees[p_colors[j]].query(p[j])
            match.append(org_num[p_colors[j]][k])
            # r = range(n)
            # if t_colors is not None and p_colors is not None:
            #    r = filter(lambda i: t_colors[i]==p_colors[j], r)
            #match.append(
            #    min(r, key=lambda i: dist(target[i], p[j]))
            #)
    if len(set(match)) != len(pattern):
        if t_colors is None and p_colors is None:
            # print "trying slow procedure"
            tree = KDTree(target)

            g = nx.DiGraph()
            for j in xrange(m):
                (d,k) = tree.query(p[j], k=5)
                for z in xrange(len(k)):
                    g.add_edge(j,m+k[z],weight=d[z])
            tmp_match = nx.max_weight_matching(g,maxcardinality=True)
            match = [None]*m
            for j in xrange(m):
                if tmp_match.has_key(j):
                    match[j] = tmp_match[j]-m
            # print match

    if None in match or len(set(match)) != len(pattern):
        # print "ERROR, bad match!", match
        return (None, None, None, None)

    aT = array([target[i] for i in match], 'f')
    aP = array(pattern,'f')
    if pattern_weights is None:
        sup = SVDSuperimposer()
        sup.set(aT, aP)
    else:
        sup = WSVDSuperimposer()
        sup.set(aT, aP, pattern_weights)
    sup.run()
    rms = sup.get_rms() # pylint: disable=E1103
    new_rot, new_tran = sup.get_rotran() # pylint: disable=E1103

    return (match, new_rot, new_tran, rms)




def find_match(target, pattern, rmsd_tolerance=0.1, all_matches=False, pattern_weights=None):
    """T is target set, P is pattern set, function returns tupple
       (matching, rotation_matrix, translation_vector) or None"""
    if len(target) < len(pattern):
        return None
    #assert len(pattern)>=3
    if len(pattern)<3:
        return None
    T = array(target, 'f')
    P = array(pattern, 'f')
    p = find_pivot_pattern(P)
    result = None
    result_rms = None

    # for all pairs of vertices of T
    for match in find_pivot_matches(T, p, rmsd_tolerance):
        rot, tran = compute_pivot_transformation(T, p, match)

        (curr_match, new_rot, new_tran, rms) = finetune_transformation(
            T, P, rot, tran,
            pattern_weights=pattern_weights)
        if curr_match is None or rms > rmsd_tolerance:
            continue

        if all_matches:
            if result is None:
                result = []
            result.append( (curr_match, new_rot, new_tran, rms) )
        else:
            if result_rms is None or result_rms > rms:
                result = (curr_match, new_rot, new_tran, rms)
                result_rms = rms
    if all_matches and result is not None:
        result.sort(key=itemgetter(3))
    return result

def progressive_match(targets, patterns, weights=None, rmsd_tolerance=0.1, all_matches=False):
    """progressive version of find match:
    first match targets[0] with patterns[0]
    then for i=1 to len(targets)-1:
      add points from targets[i] and patterns[i]
      finetune matching (discard if RMSD is too big)
    """
    assert len(targets) == len(patterns)
    if weights is None:
        weights = [1]*len(targets)
    assert len(targets) == len(weights)
    assert len(targets)>0
    # TODO point labels are required
    T = targets[0]
    P = patterns[0]
    T_colors = [0]*len(T)
    P_colors = [0]*len(P)
    W = [weights[0]]*len(P)
    result = find_match(T, P, rmsd_tolerance, True)
    if result is None:
        return None
    for i in xrange(1, len(targets)):
        T = T + targets[i]
        P = P + patterns[i]
        T_colors += [i]*len(targets[i])
        P_colors += [i]*len(patterns[i])
        W = W + [weights[i]]*len(patterns[i])
        aT = array(T, 'f')
        aP = array(P, 'f')
        new_result = []
        for (_match, rot, tran, _rms) in result:
            (new_match, new_rot, new_tran, new_rms) = finetune_transformation(
                aT, aP, rot, tran, pattern_weights=W,
                t_colors=T_colors, p_colors=P_colors
            )
            if new_match is not None and new_rms <= rmsd_tolerance:
                new_result.append([new_match, new_rot, new_tran, new_rms])
        result = new_result
    if len(result)==0:
        return None
    result.sort(key=itemgetter(3))
    if all_matches:
        return result
    else:
        return result[0]

def calculate_motifs_occurrences(fn, motifs_dir, atoms, rmsd, read_only=False, only_cache=False, verbose=True):
    (target_points,tmp_fn,progressive) = read_points(fn, atoms)

    occ_db = fn.replace(".pdb.gz","").replace(".pdb","") + ".m_occ"
    key = "d%s_a%s_r%.2f" % (os.path.basename(motifs_dir.rstrip("/")), atoms, rmsd)
    if os.path.isfile(occ_db):
        f = open(occ_db)
        db = json.load(f)
        f.close()
    else:
        if only_cache:
            return None
        db = {}
    if not db.has_key(key):
        if only_cache:
            return None
        db[key] = {}
    if verbose:
        print "calculate motifs occurrences, fn=%s occ_db=%s" % (fn, occ_db)
        print "db[%s].size=%d keys=%d" % (key, len(db[key].keys()), len(db.keys()))
    changed = 0
    for f in find_files_in_dir(motifs_dir):
        f_id = os.path.basename(f)
        if not re.match(r"^.*\.pdb(\.gz)?$",f_id):
            continue
        if not db[key].has_key(f_id):
            if verbose:
                print "calculating occurrences for motif: %s" % f
            (pattern_points,tmp_fn2,progressive2) = read_points(f, atoms)
            if progressive:
                m = progressive_match(target_points, pattern_points, rmsd_tolerance=rmsd, all_matches=True)
            else:
                m = find_match(target_points, pattern_points, rmsd, all_matches=True)
            if m is not None:
                m = [[match,rot.tolist(),tran.tolist(),rmsd] for (match,rot,tran,rmsd) in m]
            db[key][f_id] = m
            changed += 1

    if (changed>0) and (not read_only):
        if verbose:
            print "saving motif occurrences to %s (new entries: %d)" % (occ_db, changed)
        f = open(occ_db,"w")
        json.dump(db, f)
        f.close()

    return db[key]


def format_small_matrix(m, d):
    """return string with human readable representation of matrix m"""
    res = ""
    assert (d == 1 or d == 2)
    for row in m:
        if res != "":
            res += ", "
        if d == 2:
            res += "("
            i = 0
            for x in row:
                if i > 0:
                    res += ", "
                res += "%.9f" % x
                i += 1
            res += ")"
        else:
            res += "%.9f" % row
    return "[ "+ res + " ]"

def show_match(match, rot, tran, rms):
    """show human readable representation of the match"""
    if match is None:
        return
    for i in xrange(len(match)):
        print "P(%d) -> " % i,
        if match[i] is None:
            print "None"
        else:
            print "T(%d)" % match[i]

    print "RMS=%.8f" % rms
    print "rotation matrix=%s" % format_small_matrix(rot, 2)
    print "translation=%s" % format_small_matrix(tran, 1)

def save_match_model(pattern_fn, result_fn, rot_list, tran_list):
    """generate PDB file with matches
       each model contains pattern rotated and translated to match some occurrence
       in the target"""
    assert len(rot_list) == len(tran_list)
    assert len(rot_list) > 0
    parser = PDB.PDBParser()

    structure = PDB.Structure.Structure("matches-proof")

    for match_num in xrange(0, len(rot_list)):
        # reload pattern
        if re.match("^.*\.gz$", pattern_fn):
            pf = gzip.open(pattern_fn, "r")
            handle_close = True
        else:
            pf = pattern_fn
            handle_close = False
        pattern_structure = parser.get_structure("pattern", pf)
        if handle_close:
            pf.close()
        for c in pattern_structure.get_chains():
            for a in c.get_atoms():
                a.transform(rot_list[match_num], tran_list[match_num])

        # create model
        new_model = PDB.Model.Model(match_num)
        structure.add(new_model)
        for c in pattern_structure.get_chains():
            new_model.add(c)


    io = PDB.PDBIO()
    io.set_structure(structure)
    io.save(result_fn)

def save_match_pml(fn, pattern_fn, target_fn, atoms="P"):
    """save PML file loading target PDB and PDB file with all matches,
       it adds some basic visualization"""
    f = open(fn, "w")
    f.write("load %s\n" % target_fn)
    f.write("color gray80\n")
    f.write("load %s\n" % pattern_fn)
    f.write("show spheres, (name %s)\n" % atoms)
    f.write("set sphere_scale, 0.7, all\n")
    f.write("set sphere_transparency, 0.7, %s\n" % os.path.basename(target_fn))
    f.write("set sphere_transparency, 0.6, all\n")
    f.close()

def parse_args():
    """setup program options parsing"""
    parser = OptionParser()
    parser.add_option("-p", "--pattern", dest="pattern",
                  help="read pattern from FILE", metavar="FILE")
    parser.add_option("-t", "--target", dest="target",
                  help="read target from FILE", metavar="FILE")
    parser.add_option("-s", "--save-model", dest="model",
                  help="save resulting matching as model to FILE", metavar="FILE")
    parser.add_option("--rmsd-tolerance", dest="rmsd_tolerance",
                  help="maximum acceptable RMSD value, matches with greater RMSD" +
                       " will be discarded",
                  metavar="VALUE",default=1)
    parser.add_option("--atoms", dest="atoms",
                  help="comma separated list of considered atoms, default=P",
                  metavar="LIST",default="P")
    parser.add_option("--simrna-atoms", dest="simrna_atoms", action="store_true",
                  help="use set of atoms used by simrna: %s" % SIMRNA_ATOMS,
                  default=False)
    parser.add_option("--save-pml", dest="save_pml", action="store_true",
                  help="save also PML file with match visualization",
                  default=False)
    parser.add_option("--all-matches", dest="all_matches", action="store_true",
                  help="show all matches below threshold instead of the best one",
                  default=False)

    (options, _args)  = parser.parse_args()
    return (parser, options)


def main():
    """main function"""
    (parser, options) = parse_args()

    if options.pattern and options.target:
        if options.model:
            if (not re.match(".*\.pdb(.gz)?$", options.pattern) or
                not re.match(".*\.pdb(.gz)?$", options.target)):
                print "model option requires input files in PDB format (with .pdb extension)"
                parser.print_help()
                exit(1)
        if options.simrna_atoms:
            options.atoms = SIMRNA_ATOMS
        (P, _p_name, progressive) = read_points(options.pattern, options.atoms)
        (T, _t_name, _tmp) = read_points(options.target, options.atoms)
        if progressive:
            res = progressive_match(T, P,
                    rmsd_tolerance=float(options.rmsd_tolerance),
                    all_matches=options.all_matches
                  )
        else:
            res = find_match(T, P, float(options.rmsd_tolerance), options.all_matches)
        if res:
            if options.all_matches:
                i = 0
                for r in res:
                    i += 1
                    (match, rot, tran, rms) = r
                    print "Match %d with RMSD=%.5f has been found" % (i, rms)
                    show_match(match, rot, tran, rms)
                if options.model:
                    rotations = []
                    translations = []
                    for r in res:
                        rotations.append(r[1])
                        translations.append(r[2])
                    save_match_model(options.pattern, options.model, rotations, translations)
            else:
                (match, rot, tran, rms) = res
                print "Match with RMSD=%.5f has been found" % rms
                show_match(match, rot, tran, rms)
                if options.model:
                    save_match_model(options.pattern, options.model, [rot], [tran])
            if options.save_pml:
                pml_fn = re.sub(".pdb(.gz)?$","", options.model) + ".pml"
                save_match_pml(pml_fn, os.path.realpath(options.model), os.path.realpath(options.target))
        else:
            print "No matches detected!"
    else:
        print "specify Pattern and Target"
        parser.print_help()
        exit(1)

if __name__ == '__main__':
    main()
