#!/usr/bin/env python
"""
Compare structures
"""
import random
import sys
import itertools
import os
import math
from optparse import OptionParser
from itertools import product
from Bio.SVDSuperimposer import SVDSuperimposer
from numpy import dot, array
from scipy.spatial import KDTree
import networkx as nx
from progressbar import ProgressBar, Percentage, Bar, ETA

from substructure_match import read_points, save_match_model, find_match, progressive_match, calculate_motifs_occurrences
from sm_utils import save_structure, dist, SIMRNA_ATOMS, find_files_in_dir

VERSION = "0.2"
DEFAULT_ATOMS = "P"

def status(msg):
    """display status message"""
    print msg

def parse_args():
    """setup program options parsing"""
    parser = OptionParser(version="%prog FILE1 FILE2" + VERSION)
    parser.add_option("--rmsd-tolerance", dest="rmsd_tolerance",
                  help="maximum acceptable RMSD value, matches with greater RMSD" +
                       " will be discarded", 
                  metavar="VALUE",default=1)
    parser.add_option("--atoms", dest="atoms",
                  help="comma separated list of considered atoms, default=P",   
               
                  metavar="LIST",default=DEFAULT_ATOMS)
    parser.add_option("--simrna-atoms", dest="simrna_atoms", action="store_true",
                  help="use set of atoms used by simrna: %s" % SIMRNA_ATOMS, 
                  default=False)
    parser.add_option("--local", dest="local_align", action="store_true",
                  help="use local alignment", 
                  default=False)
    parser.add_option("-m", "--motifs", dest="motifs",    
                 help="use motifs based comparison, read motifs from given directory", metavar="DIR")
    parser.add_option("-o", "--output", dest="output",
                 help="save the result to file", metavar="FILE")

    (options, args)  = parser.parse_args()

    error = None
    if len(args) != 2:
        error = "Two arguments required"
    if error is not None:
        print "ERROR: %s" % error
        parser.print_help()
        sys.exit(1)
    options.args0 = args[0]
    options.args1 = args[1]

    return (parser, options, args)

def find_alignment(tree1, inp1, inp2, idx1, idx2, rmsd_tolerance=0.1):
    n2 = len(inp2)
    sup = SVDSuperimposer()
    sup.set(array([inp1[i] for i in idx1],'f'), array([inp2[i] for i in idx2],'f'))
    sup.run()
    rms = sup.get_rms()
    if rms>rmsd_tolerance:
        return [[],[],rms,None,None]
    rot, tran = sup.get_rotran()

    mod_inp2 = dot(array(inp2,'f'), rot) + tran
    
    # print "rms=%s" % rms
    res_idx1 = []
    res_idx2 = []
    for i in xrange(n2):
        (d,k) = tree1.query(mod_inp2[i])
        if d>rmsd_tolerance:
            continue
        res_idx1.append(k)
        res_idx2.append(i)
        # print "d=%s k=%s" % (d,k)

    sup.set(array([inp1[i] for i in res_idx1],'f'), array([inp2[i] for i in res_idx2],'f'))
    sup.run()
    rms = sup.get_rms()
    rot, tran = sup.get_rotran()
    if rms>rmsd_tolerance:
        return [[],[],rms,None,None]

    return [res_idx1, res_idx2, rms, rot, tran]
    

class PartialMatches:    

    def __init__(self,n1,n2):
        self.n1=n1
        self.n2=n2
        self.matches=[]
        self.m_dict={}

    def add_match(self,m1,m2):
        key = str((m1,m2))
        if self.m_dict.has_key(key):
            return False
        num = len(self.matches)
        self.m_dict[key] = num
        self.matches.append([m1,m2])
        return True

    def _gen_conflict_graph(self):
        n = len(self.matches)
        weights = [len(m[0]) for m in self.matches]
        adj = [[] for i in xrange(n)]
        for i in xrange(n-1):
            for j in xrange(i+1,n):
                if (len(set(self.matches[i][0]).intersection(set(self.matches[j][0])))!=0 or 
                    len(set(self.matches[i][1]).intersection(set(self.matches[j][1])))!=0):
                    adj[i].append(j)
                    adj[j].append(i)
        return (weights, adj)

    def _weighted_max_independent_set(self,weights, adj):
        assert len(weights) == len(adj)
        n = len(weights)
        best_res = []
        best_w = -1
        for i in xrange(100):
            valid = set(range(n))            
            available = n
            res = []
            w = 0
            while available>0:
                x = random.sample(valid,1)[0]
                res.append(x)
                w += weights[x]
                valid.remove(x)
                valid = valid.difference(set(adj[x]))
                available = len(valid)
            if w>best_w:
                print "independent set with weight %s" % w
                best_w = w
                best_res = res
        return (best_w, best_res)

    def combine_matches(self):
        (w, adj) = self._gen_conflict_graph()
        (weight, m) = self._weighted_max_independent_set(w, adj)        
        return [weight, [self.matches[i] for i in m]]

def compare_structures_local(inp1, inp2, rmsd_tolerance=0.1):
    n1 = len(inp1)
    n2 = len(inp2)
    print "n1=%d n2=%d" % (n1, n2)
    tree1 = KDTree(inp1)
    T = rmsd_tolerance*rmsd_tolerance
    m = PartialMatches(n1,n2)
    for (i1, j1) in product(xrange(n1), xrange(n2)):
        for (i2, j2) in product(xrange(n1), xrange(n2)):
            if i1==i2 or j1>=j2:
                continue
            inp1_d12 = dist(inp1[i1], inp1[i2])
            inp2_d12 = dist(inp2[j1], inp2[j2])
            if abs(inp1_d12-inp2_d12)>=T:
                continue
            # print "(%d,%d) <-> (%d,%d)" % (i1,i2,j1,j2)        
            for (i3, j3) in product(xrange(n1), xrange(n2)):
                if i1==i3 or j1>=j3 or i2==i3 or j2>=j3:
                    continue
                inp1_d13 = dist(inp1[i1], inp1[i3])
                inp1_d23 = dist(inp1[i2], inp1[i3])
                inp2_d13 = dist(inp2[j1], inp2[j3])
                inp2_d23 = dist(inp2[j2], inp2[j3])
                if abs(inp1_d13-inp2_d13)>=T:
                    continue
                if abs(inp1_d23-inp2_d23)>=T:
                    continue
                (ii1,jj1,rms,rot,tran) = find_alignment(tree1, inp1, inp2, [i1,i2,i3], [j1,j2,j3], rmsd_tolerance)
                if len(ii1)<=3:
                    continue
                if not m.add_match(ii1,jj1):
                    continue
                print "(%d,%d,%d) <-> (%d,%d,%d), score: %d, r: %s" % (i1,i2,i3,j1,j2,j3, len(ii1), (ii1,jj1))
    print m.combine_matches()

def gen_pivots(points,ordered=False):
    n = len(points)
    tree = KDTree(points)
    res = []
    for i in xrange(n):
        (d,p) = tree.query(points[i], k=7)
        for (j,k) in itertools.permutations(p, 2):
            if i in (j,k):
                continue
            if ordered:
                if i>j or i>k or j>k:
                    continue
            dij = dist(points[i], points[j])
            dik = dist(points[i], points[k])
            djk = dist(points[j], points[k])
            d_sum = dij+dik+djk
            res.append((d_sum,[dij,dik,djk],[i,j,k]))
    return sorted(res)

def compare_distances(d1,d2,tolerance):
    diff = 0
    for i in xrange(3):
        diff += abs(d1[i]-d2[i])
    return (diff < tolerance)

def compare_structures_global(inp1, inp2, options=None, rmsd_tolerance=0.1):
    n1 = len(inp1)
    n2 = len(inp2)
    print "n1=%d n2=%d" % (n1, n2)

    tree1 = KDTree(inp1)
    T = rmsd_tolerance*rmsd_tolerance

    best_value = -1
    best_res = None
    best_rms = None
    best_tau = None

    pivots1 = gen_pivots(inp1)
    p1_count = len(pivots1)
    pivots2 = gen_pivots(inp2,True)
    p2_count = len(pivots2)
    print "pivots1=%d pivots2=%d" % (len(pivots1), len(pivots2))

    left = 0
    right = 0
    D_SUM_TOLERANCE = rmsd_tolerance
    D_TOLERANCE = rmsd_tolerance

    widgets = ['Comparing: ', Percentage(), ' ', Bar(), ' ', ETA()]
    pbar = ProgressBar(widgets=widgets, maxval=p1_count).start()
    
    for p1 in xrange(p1_count):
        while left<p2_count and pivots2[left][0] < pivots1[p1][0]-D_SUM_TOLERANCE:
            left += 1
        right = left
        while right<p2_count-1 and pivots2[right+1][0] < pivots1[p1][0]+D_SUM_TOLERANCE:
            right += 1
        if left>=p2_count:
            break
        # print "p1=%d p2 in (%d,%d)" % (p1,left,right)
        for p2 in xrange(left, right+1):
            if not compare_distances(pivots1[p1][1], pivots2[p2][1], D_TOLERANCE):
                continue
            (i1,i2,i3) = pivots1[p1][2]
            (j1,j2,j3) = pivots2[p2][2]
            (ii1,jj1,rms,rot,tran) = find_alignment(tree1, inp1, inp2, [i1,i2,i3], [j1,j2,j3], rmsd_tolerance)
            curr_val = len(ii1)
            if curr_val>3 and curr_val > best_value:
                best_value = curr_val
                best_res = (ii1,jj1)
                best_rms = rms
                best_tau = (rot,tran)
                print "alignment of size %d: rms=%s %s" % (best_value, rms, best_res)
                if options is not None and options.output:
                    save_match_model(options.args1, options.output, [rot], [tran])
        pbar.update(p1)
        
    pbar.finish()
    if best_value==-1:
        return None
    # print (best_value, best_res, best_rms)
    return (best_value, best_res, best_rms, best_tau)

def calculate_result_score(inp1,inp2,res):
    n1 = len(inp1)
    n2 = len(inp2)
    if res is None:
        return 0    
    return (float(2*res[0]))/(n1+n2)
    return (float(res[0]))/min(n1,n2)

def compare_using_motifs(args, options):
    rmsd = float(options.rmsd_tolerance)
    m1 = calculate_motifs_occurrences(args[0], options.motifs, options.atoms, rmsd)
    m2 = calculate_motifs_occurrences(args[1], options.motifs, options.atoms, rmsd)

    count1 = 0
    sum1 = 0
    for k1 in m1.keys():
        if m1[k1]:
            m_size = len(m1[k1][0][0])
            if m_size<4:
                continue
            count1 += 1
            sum1 += math.sqrt(len(m1[k1]))
    count2 = 0
    sum2 = 0
    for k2 in m2.keys():
        if m2[k2]:
            m_size = len(m2[k2][0][0])
            if m_size<4:
                continue
            count2 += 1
            sum2 += math.sqrt(len(m2[k2]))
    count12 = 0
    count21 = 0
    sum12 = 0
    sum21 = 0
    for k1 in m1.keys():
        if m1[k1] and m2.has_key(k1) and m2[k1]:
            m_size = len(m1[k1][0][0])
            if m_size<4:
                continue
            count12 += 1
            sum12 += math.sqrt(len(m1[k1]))
            print "motif12: %s (%d), motif size: %s" % (k1, len(m1[k1]), m_size)
    for k2 in m2.keys():
        if m2[k2] and m1.has_key(k2) and m1[k2]:
            m_size = len(m2[k2][0][0])
            if m_size<4:
                continue
            count21 += 1
            sum21 += math.sqrt(len(m2[k2]))
            print "motif21: %s (%d), motif size: %d" % (k2, len(m2[k2]), m_size)

    print "count1=%d count2=%d" % (count1, count2)
    print "count12=%d sum12=%d count21=%d sum21=%d" % (count12, sum12, count21, sum21)
    if count1>3 and count2>3 and min(count12,count21)>3:
        score = float(sum12+sum21)/max(1,(sum1+sum2))
    else:
        score = 0
    print "SCORE: %.5f" % score

def main():
    """main function"""
    (parser, options, args) = parse_args()
        
    if options.simrna_atoms:
        options.atoms = SIMRNA_ATOMS

    rmsd = float(options.rmsd_tolerance)

    if options.motifs:
        compare_using_motifs(args, options)
        return 
    
    (inp1,name1,prog1) = read_points(args[0], options.atoms)
    (inp2,name2,prog2) = read_points(args[1], options.atoms)

    if options.local_align:
        compare_structures_local(inp1, inp2, rmsd_tolerance=rmsd)
        if options.output:
            print "output option not supported yet"
    else:

        res = compare_structures_global(inp1, inp2, options, rmsd_tolerance=rmsd)    
        print "SCORE: %.5f" % calculate_result_score(inp1, inp2, res)
        if options.output and res is not None:
            (score, matching, rms, tau) = res
            save_match_model(args[1], options.output, [tau[0]], [tau[1]])


if __name__ == '__main__':
    main()
