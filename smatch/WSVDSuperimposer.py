"""
Weighted SVD Superimposer
(slightly modified version of BIO.SVDSuperimposer)
"""
#pylint: disable=E0611,E1101,E1102,E1121

from numpy import dot, transpose, sqrt, diag
from numpy.linalg import svd, det

from Bio.SVDSuperimposer import SVDSuperimposer 

# W0232: Class has no __init__ method
class WSVDSuperimposer(SVDSuperimposer): # pylint: disable=W0232
    """Modified SVDSuperimposer with point weights support"""
    
    def _clear(self):
        "clear the state"      
        self.weights = None # pylint: disable=W0201
        SVDSuperimposer._clear(self)
        
        
    def set(self, reference_coords, coords, weights=None):
        "Initialize the points and weights"
        SVDSuperimposer.set(self, reference_coords, coords)
        self.weights = weights # pylint: disable=W0201
        
    def _rms(self, coords1, coords2):
        "Return rms deviations between coords1 and coords2."
        if self.weights is None:
            return SVDSuperimposer._rms(self, coords1, coords2)
        diff = coords1-coords2
        l = coords1.shape[0]
        diff2 = sum(diff*diff)
        return sqrt(sum([diff2[i]*self.weights[i] for i in xrange(len(diff2))])/l)
        
    def run(self):
        "Superimpose the coordinate sets."
        if self.weights is None:
            return SVDSuperimposer.run(self)
        if self.coords is None or self.reference_coords is None:
            raise Exception("No coordinates set.")
        coords = self.coords
        reference_coords = self.reference_coords
        weights = self.weights
        # center on centroid
        av1 = sum([weights[i]*coords[i] for i in xrange(len(coords))]) / sum(weights)
        av2 = sum([weights[i]*reference_coords[i] 
                     for i in xrange(len(reference_coords))]) / sum(weights)    
        coords = coords-av1
        reference_coords = reference_coords-av2
        # correlation matrix
        a = dot(transpose(coords), dot(diag(weights), reference_coords))
        u, _d, vt = svd(a)
        self.rot = transpose(dot(transpose(vt), transpose(u))) # pylint: disable=W0201
        # check if we have found a reflection
        if det(self.rot)<0:
            vt[2] = -vt[2]
            self.rot = transpose(dot(transpose(vt), transpose(u))) # pylint: disable=W0201
        self.tran = av2-dot(av1, self.rot) # pylint: disable=W0201



