"""
Utils
"""

import os
import re
import math
from Bio import PDB
import cPickle
import hashlib
import gzip
import simplejson

CACHE_DIR = ".cache"
SIMRNA_ATOMS = "P|C4',N1,N9"

def save_structure(structure, filename):
    """save structure to PDB file"""
    io = PDB.PDBIO()
    io.set_structure(structure)
    io.save(filename)

def load_structure(filename):
    """parse PDB file"""
    if filename.split('.')[-1] == 'gz':
        f = gzip.open(filename, "r")
    else:
        f = open(filename, "r")
    res = PDB.PDBParser().get_structure("structure", f)
    f.close()
    return res

def move_to_0(structure):
    """translate all atoms, such that first atom is at 0,0,0"""
    first_atom = list(structure.get_atoms())[0]
    t = first_atom.get_coord()
    for a in structure.get_atoms():
        a.set_coord(a.get_coord()-t)

def dist(p1, p2):
    return math.sqrt(sq_dist(p1, p2))

def sq_dist(p1, p2):
    assert(len(p1)==len(p2))
    assert(len(p1)==3)
    return ((p1[0]-p2[0])*(p1[0]-p2[0]) +
            (p1[1]-p2[1])*(p1[1]-p2[1]) +
            (p1[2]-p2[2])*(p1[2]-p2[2]))


# files and directories functions

def find_files_in_dir(dirname):
    """find all files in directory dirname (and its subdirectories)"""
    res = []
    for root, _dirs, files in os.walk(dirname):
        for f in files:
            res.append(os.path.join(root, f))            
    return res    

def cache_init():
    if not os.path.exists(CACHE_DIR):
        os.mkdir(CACHE_DIR)
  
def cache(prefix):
    """cache decorator"""
    def add_cache(f):
        def new_f(*args):
            key = hashlib.md5(str(args)).hexdigest()
            file_name = os.path.join(CACHE_DIR, prefix + '_' + key)

            try:
                return cPickle.load(open(file_name))
            except IOError:
                value = f(*args)
                cPickle.dump(value, open(file_name, 'w'))
            return value
        return new_f
    
    return add_cache

def pickle_save(fn, value):
    cPickle.dump(value, open(fn, 'w'))

def pickle_load(fn):
    if not os.path.isfile(fn):
        return None
    return cPickle.load(open(fn))

def load_json(fn):
    if re.match(".*.gz$",fn):
        f = gzip.open(fn)
    else:
        f = open(fn)
    return simplejson.load(f)

def save_json(fn,data,indent=None):
    if re.match(".*.gz$",fn):
        f = gzip.open(fn,"w")
    else:
        f = open(fn,"w")
    return simplejson.dump(data,f,indent=indent)