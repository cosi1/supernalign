#!/usr/bin/env python

from StringIO import StringIO
from Bio.PDB import PDBParser, Model, Structure, PDBIO
from global_config import SUPERALN_DIR, SUPERIMPOSER_PATH, X3DNA_PATH
from superimposer import Superimposer, SuperimposerException
import os, glob, sys, subprocess


class ARTS_Superimposer(Superimposer):
    """
    Superimposes Fragments using ARTS
    """
    
    def impose(self, model_id1, chain_id1, model_id2, chain_id2):
        """
        Imposes Fragments 
        """

        coords1 = self.get_dists(self.pdb_data_A, False) #coords before superposition
        name1, name2, pdb1, pdb2 = self.pdb_temp_files()

        ARTS_OUTPUT_A = os.path.join(self.TEMP_DIR, "arts_" + name1) # without extension
        ARTS_OUTPUT_PDB_A = ARTS_OUTPUT_A + ".pdb"
        ARTS_OUTPUT_B = os.path.join(self.TEMP_DIR, "arts_" + name2) # without extension
        ARTS_OUTPUT_PDB_B = ARTS_OUTPUT_B + ".pdb"

        arts_sh = os.path.join(SUPERALN_DIR, "arts.sh")

        command = " ".join((arts_sh,
                            name1, name2,
                            X3DNA_PATH,
                            self.TEMP_DIR,
                            SUPERIMPOSER_PATH,
                            os.path.dirname(os.path.realpath(__file__))))

        nullDev = open(os.devnull, "w")
        errCode = subprocess.call(command, stdout = nullDev, stderr = nullDev, shell = True)

        if not os.path.exists(ARTS_OUTPUT_PDB_A) or not os.path.exists(ARTS_OUTPUT_PDB_B):
            self.pdb_data_out = [self.pdb_data_A, self.pdb_data_B] 	    
            #print "ARTS: Fragment not aligned!" #XXX
        else:
            list_of_structures = []
        
            parser = PDBParser()
            structure_out_A = parser.get_structure("arts", ARTS_OUTPUT_PDB_A)
            model_out_A = structure_out_A[0]
            chain_out_A = model_out_A[chain_id1]
            chain_out_A.id = "A"
            structure_out_B = parser.get_structure("arts", ARTS_OUTPUT_PDB_B)
            model_out_B = structure_out_B[0]
            chain_out_B = model_out_B[chain_id2]
            chain_out_B.id = "B"
            list_of_structures = (structure_out_A, structure_out_B)
        
            '''
            coords2 = self.get_dists(list_of_structures[0], True) #coords after superposition
            structs_coords_edited = self.update_coords(coords1, coords2, list_of_structures)

            for struct in structs_coords_edited:
            '''
            for struct in list_of_structures:
                pio = PDBIO()
                fragment_file = StringIO()
                pio.set_structure(struct)
                pio.save(fragment_file)
                self.pdb_data_out.append(fragment_file.getvalue())

        # remove temporary files
        temp_files = os.listdir(self.TEMP_DIR)
        for f in temp_files:
            os.remove(os.path.join(self.TEMP_DIR, f))

