#!/bin/bash

# args:
# $1 - absolute path to SARA
# $2 - first input file
# $3 - first chain ID
# $4 - second input file
# $5 - second chain ID
# $6 - output .pdb file (without extension)

export X3DNA=$1/x3dna
export PATH=$PATH:$X3DNA/bin

$1/runsara.py $2 $3 $4 $5 -p $6 -b -s

