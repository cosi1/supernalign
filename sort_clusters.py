#!/usr/bin/env python

# Created on Mar 12, 2017 @author: dorota
    
import sys
from itertools import groupby
from operator import itemgetter

def sort_clusters(clusters):
        ourDict = {}
        # read files and get rid of the first character
        mydata = clusters
        #mydata =[line.strip().split('\t') for line in open(clusters)]
        for i in range(0,len(mydata)):
            for j in range(0,len(mydata[i])):
                makeKey = mydata[i][j][1:]
                myKey = makeKey.strip()
                ourDict[myKey] = mydata[i][j]
                mydata[i][j]=int(mydata[i][j][1:])  # get rid of the first character and convert to integer
        print "[our Keys]"
        for key in ourDict:
            print key, 'corresponds to', ourDict[key]          

        # sort the list
        for i in range(0,len(mydata)):
            mydata[i].sort()
        print "[mydata]"
        for i in range(0,len(mydata)):
            print mydata[i]
        print ""

        # find split each sequence in longest continuous sequences
        newlist=[]
        for ii in range(0,len(mydata)):
            myelements=[]
            for k, g in groupby(enumerate(mydata[ii]), lambda (i,x):i-x):
                myelements.append( map(itemgetter(1),g))
            newlist.append(myelements)
        print "[new list]"
        for i in range(0,len(newlist)):
            print newlist[i]
        print ""

        #find sequences that are smaller than 3 elements and put them in a new double list. The same for the rest
        redundantElements = []
        singleList = []
        for i in range(0,len(newlist)):
            myelements2 = []
            for j in range(0,len(newlist[i])):
                if len(newlist[i][j]) <=3:
                    redundantElements.append(newlist[i][j])
                else:
                    myelements2.append(newlist[i][j])
            singleList.append(myelements2)
                                                                        


        print "[single list after removing redundants]"
        for i in range(0,len(singleList)):
            print singleList[i]
        print ""

        print "[redundants]"
        for i in range(0,len(redundantElements)):
            print redundantElements[i]
        print ""

        # We are sorting here the redundants according to the first element of the list
        redundantElements.sort(key=lambda x: x[0])     # this line

            
        # We are now trying to find in which list of the original data of the new list do the redundant elements fit
        foundInsingleList=[]
        for i in range(0,len(redundantElements)):
            firstElementOfRedundants = redundantElements[i][0]
            lastELementOfRedundants = redundantElements[i][-1]
            for mm in range(1,1000):
                templist = []
                maxLength = 0
                for j in range(0,len(singleList)):
                    for k in range(0,len(singleList[j])):
                        for l in range(0,len(singleList[j][k])):
                            if abs(firstElementOfRedundants - singleList[j][k][l])  == mm or abs(lastELementOfRedundants - singleList[j][k][l]) == mm:    # change from 22.06.2017
                                myLength=len(singleList[j][k])
                                if myLength > maxLength:
                                    maxLength=myLength
                                    templist=[j,k]
                if len(templist)>0:
                    break
            redundantElements[i].append(templist)


        print "[redundants after checking where it fits]"
        for i in range(0,len(redundantElements)):
            print redundantElements[i]
        print ""

        # Add the redundant elements to the correct singleList                        
        print "[foundInsingleList biggest cluster and where to put it]" 
        for i in range(0,len(redundantElements)):
            newElement = []
            if len(redundantElements[i]) > 2:
                for j in range(0,len(redundantElements[i])-1):
                    mynew = redundantElements[i][j]
                    newElement.append(mynew)
            else:
                mynew = redundantElements[i][0]
                newElement.append(mynew)
            singleList[redundantElements[i][-1][0]][redundantElements[i][-1][1]].extend(newElement)
            print newElement,redundantElements[i], singleList[redundantElements[i][-1][0]][redundantElements[i][-1][1]]
        print ""     

        # Now merge and sort the list.
        MymergeList = []
        for i in range(0,len(singleList)):
            againNew = [x for sublist in singleList[i] for x in sublist]
            MymergeList.append(againNew) 

        print "[Merged list ]" 
        print MymergeList

        for i in range(0,len(MymergeList)):
            MymergeList[i].sort()   
        print ""
        print "[Sorted and Merged list ]" 
        print MymergeList                      
        clusters= MymergeList
      

        # adding the letter
        for i in range(0,len(clusters)):
            for j in range(0,len(clusters[i])):
                clusters[i][j] = ourDict[str(clusters[i][j])]
        
        return clusters
